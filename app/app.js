/**
 * Created by VenoM on 16.03.2017.
 */



var app = angular.module("myApp", ['angularCSS', 'ngAnimate', 'ngTable', 'ngRoute', 'mobile-angular-ui','ui.router']);


app.config(function ($stateProvider, $urlRouterProvider, fooProvider) {
    fooProvider.setPrivate('New value from config');
    // fooProvider.get();
    var bla = MyCtrl();
    $stateProvider
        .state('Graph', {
            url: '/Graph',
            views: {
                "body": {
                    templateUrl: "pure-css-accordion-nav/google-chart.html"
                },
                "PageHeader": {
                    templateUrl: "user_components/uiTemplates/Graph/PageHeaderTemplateGraph.html"
                },
                "sidebarLeft": {
                    templateUrl: "user_components/uiTemplates/Graph/SidebarLeftTemplateGraph.html"
                }
            },
        })
        .state('History', {
            url: '/History',
            views: {
                "body": {
                    templateUrl: "pure-css-accordion-nav/log-template.html"
                },
                "PageHeader": {
                    templateUrl: "user_components/logTable/PageHeaderTemplateHistory.html"
                },
                "sidebarLeft": {
                    templateUrl: "user_components/logTable/logTableTemplateControl.html"
                }
            }
        })
        .state('Sensors', {
            url: '/Sensors',
            views: {
                "body": {
                    templateUrl: "user_components/uiTemplates/Sensors/BodyTemplateSensors.html"
                },
                "PageHeader": {
                    templateUrl: "user_components/uiTemplates/Sensors/PageHeaderTemplateSensors.html"
                },
                "sidebarLeft": {
                    templateUrl: "user_components/uiTemplates/Sensors/SidebarLeftTemplateSensors.html"
                }
            }
        })
        .state('Settings', {
            url: '/Settings',
            views: {
                "body": {
                    templateUrl: "user_components/settingBkm/settingBkmTemplate.html"
                },
                "PageHeader": {
                    templateUrl: "user_components/settingBkm/settingBkmTemplateHeader.html"
                },
                "sidebarLeft": {
                    templateUrl: "user_components/settingBkm/settingBkmTemplateSdbrleft.html"
                }
            },
        })
        .state('catalog.category.product', {
            url: '/:pId',
            views: {
                '@': {
                    template: function($stateParams) {
                        return '<div>Product:' + $stateParams.pId + '</div>';
                    },
                    controller: function() {}
                }
            }
        });
})

app.provider('foo', function() {
    var thisIsPrivate = "Private";
    return {
        setPrivate: function(newVal) {
            thisIsPrivate = newVal;
        },
        $get: function() {
            function getPrivate() {
                return thisIsPrivate;
            }
            return {
                variable: "This is public",
                getPrivate: getPrivate
            };
        }
    };
});

function MyCtrl($scope, globalVars, foo)
{
    return $scope;
    // return $scope.isDirtySid = "myService = " + foo;
}


// app.config(function($stateProvider, $urlRouterProvider) {
//     $stateProvider
//         .state('Graph', {
//         url: '/Graph',
//         // templateUrl: "user_components/logTable/logTableTemplate.html",
//         // templateUrl: "pure-css-accordion-nav/google-chart.html",
//         views: {
//             "body": {
//                 templateUrl: "pure-css-accordion-nav/google-chart.html"
//             },
//             "PageHeader": {
//                 // template: $scope.globalVars.language,
//                 template: "График"
//             }
//         },
//         })
//         .state('History', {
//             url: '/History',
//             // templateUrl: "pure-css-accordion-nav/log-template.html",
//             // templateUrl: "index-async.html",
//             views: {
//                 "body": {
//                     templateUrl: "pure-css-accordion-nav/log-template.html"
//                 },
//                 "PageHeader": {
//                     template: "History"
//                 }
//             }
//         })
//         .state('catalog.category.product', {
//             url: '/:pId',
//             views: {
//                 '@': {
//                     template: function($stateParams) {
//                         return '<div>Product:' + $stateParams.pId + '</div>';
//                     },
//                     controller: function() {}
//                 }
//             }
//         });
// });

app.provider('globalVars', globalVars);




