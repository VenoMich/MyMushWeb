/**
 * Created by VenoM on 19.04.2017.
 */
app.factory('sendRelaySetReq', ['$http', function($http) {
    // return $http.get('https://s3.amazonaws.com/codecademy-content/courses/ltp4/forecast-api/forecast.json')
    return $http.get('http://localhost:8888/setRelay')
        .success(function(data) {
            return data;
        })
        .error(function(err) {
            return err;
        });
}]);
