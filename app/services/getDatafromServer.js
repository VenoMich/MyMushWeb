/**
 * Created by VenoM on 29.03.2017.
 */
var language = "Russian";
// var language = "English";

var userInfo={}
userInfo["Customer_ID"] = "";
userInfo["Login"] = "";
userInfo["Password"] = "";
userInfo["ServerIP"] = 'http://localhost:'
userInfo["ServerPort"] = "81/";


var httpGet={}
//********************SET PARAM********************************************
httpGet["setSensorName"] = "setSensorName";
httpGet["setLowerValue"] = "setLowerValue";
httpGet["setUpperValue"] = "setUpperValue";
httpGet["setDelta"] = "setDelta";
httpGet["setRFDSleepTime"] = "setRFDSleepTime";
httpGet["setRelay"] = "setRelay";
//********************GET CUR DATA********************************************
httpGet["getBanStatus"] = "getBanStatus";
httpGet["getAccauntInfo"] = "getAccauntInfo";
httpGet["getMasters"] = "getMasters";// get active Masters (ID, last update)
httpGet["getSlaves"] = "getSlaves";//get active Slaves (Master_ID, Slave_ID, Battery_V if RFD, last update)
httpGet["getRelays"] = "getRelays";//get relay status (Master_ID, Slave_ID, Statusm last update)
httpGet["getSensors"] = "getSensors";//get sensors status (Master_ID, Slave_ID, Type, Address, Value, last update)
//********************GET LOG********************************************
httpGet["getMasterLog"] = "getMasterLog";
httpGet["getSlaveLog"] = "getSlaveLog";
httpGet["getSensorsLog"] = "getSensorsLog";
//********************GET data for CHARTS********************************************
httpGet["getHudmChartData"] = "getHudmChartData";
httpGet["getTempChartData"] = "getTempChartData";



