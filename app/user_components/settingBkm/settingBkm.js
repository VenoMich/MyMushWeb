/**
 * Created by VenoM on 03.11.2020.
 */
app.component('settingBkm',{
    restrict: 'E',
    bindings:{
        settingBkm: '=',
        settingBkmParams:'=',
        data: '=',
        photo: '=',
        scope: '=',
    },

        controller: function($scope, $element,globalvars){
        $scope.glovalVars=globalvars;

    },
    templateurl: "user_components/settingBkm/SettingBkmTemplate.html",
},)