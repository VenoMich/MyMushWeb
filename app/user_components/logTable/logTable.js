/**
 * Created by VenoM on 29.10.2017.
 * Component based on angular ngTable
 * input: logData, images
 * output: table template
 */
app.component('logTable',  {
    restrict: 'E',
    bindings: {
        logtable: '=',
        tableParams: '=',
        data: '=',
        photo: '=',
        scope: '=',
    },
    controller: function($scope, $element, globalVars) {
        $scope.globalVars = globalVars;
        if ($scope.globalVars.language == "English"){
            $scope.showCol = [
                { field: "masterID", title: "Master device", show: false },
                { field: "node", title: "Node", show: false },
                { field: "value", title: "Value", show: true },
                { field: "event", title: "Event", show: true },
                { field: "minThresh", title: "Min threshold", show: false },
                { field: "maxThresh", title: "Max threshold", show: false },
            ];
        }
        if ($scope.globalVars.language == "Russian"){
            $scope.showCol = [
                { field: "masterID", title: "Гл. прибор", show: false },
                { field: "node", title: "Узел", show: false },
                { field: "value", title: "Знач.", show: true },
                { field: "event", title: "Событие", show: true },
                { field: "minThresh", title: "Мин. порог", show: false },
                { field: "maxThresh", title: "Макс. порог", show: false },
            ];
        }
        $scope.$on('updateLogTableLanguage', function(){
            var bla = 0;
        })

        // var $ctrl = this;
        // $ctrl.count = 0;
        // $ctrl.isParentPressed = false;
        // var remote = {
        //     remoteFunc: function () {
        //         $ctrl.isParentPressed = true;
        //         $ctrl.count++;
        //     }
        // }
        // $ctrl.remoteReady({remote: remote});
        // performAction = function(action, id, type) {
        //     if (action === 'cancel') {
        //         event.state = 'canceling';
        //     }
        // };
        // $scope.testClick = function(action, id, type) {
        //     if (action === 'expandGroup') {
        //         $scope.globalVars.ngTableExpanded = type;
        //         return;
        //     }
        //     this.data = $scope.globalVars.broodLog;
        //     if (action == 'clickNgTable' && $scope.globalVars.ngTableExpanded != "" && id.$$ngAnimateParentKey == undefined){
        //         for(i = 0; i < this.$groups.length; i++){
        //             this.$groups[i].$hideRows = $scope.globalVars.ngTableExpanded[i].$hideRows;
        //         }
        //         this.data = $scope.globalVars.broodLog;
        //         return this.$groups;
        //     }
        // };
    },
    templateUrl: "user_components/logTable/logTableTemplate.html",
})