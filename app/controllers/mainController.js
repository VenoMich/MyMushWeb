/**
 * Created by VenoM on 28.03.2017.
 */
app.controller('MainController', function MainController($scope, $http, globalVars) {
    $scope.inputtextLogin = ""; // user login inputtext
    $scope.inputtextPassword = ""; // user password inputtext
    $scope.text = "";//login menu text
    $scope.index = "";
    $scope.ngshowLogin = "";//flag of show login inputtext
    $scope.ngshowlogapp = true;//flag of show main page
    $scope.globalVars = globalVars;
    var conf={// http req param
        timeout: 600, // 600 миллисекунд
        data: $scope.func,
        withCredentials: true,
        login:    $scope.inputtextLogin,
        password: $scope.inputtextPassword,
        ban: false,
        bantime: 0
    };
    getBanStatus($http, $scope, conf);//check for ban status
    $scope.users = {
        text: 'Какой js-фреймворк лучше использовать?',
        author: 'Иван Иванов',
        date: '20/10/2013',
        answers:
            [
                {
                    name: "vasya",
                    login: "vas",
                    password: "1"
                },
                {
                    name: "petya",
                    login: "pet",
                    password: "2"
                }
            ]
    };
    $scope.testngShow = function(){
        if ($scope.ngshowlogapp == false){
            return false;
        }
        else{
            return true;
        }
    }

});

//get ban status
function getBanStatus($http, $scope, conf)
{
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + httpGet.getBanStatus; // host + req string
    if (conf == "")
    {
        var conf={
            timeout: 600, // 600 миллисекунд
            data: "",
            withCredentials: true,
            ban: false,
            bantime: 0
        };
    }

    $http({
        method: "PUT",
        url: address,
        data: conf,
    }).then(function(banStatus) {
        //************if data received good - check pass and login in database***********
        if (banStatus.data.banStatus == true)
        {
            conf.ban = true;
            conf.bantime = banStatus.banTime;
            $scope.text = "YOU ARE BANNED FOR " + conf.bantime + " minutes!";
            alert("Your are banned! Ban elapsed in "+ conf.bantime+" minutes.");
        }
        if (banStatus.data.banStatus == false)//if ban false --> send req for accInfo
        {
            /*********************************************************************
             * Function:         Mainbutton = function(){}
             * PreCondition:     myMush server started
             * Input:            Login, password
             * Return^           data or error
             * Overview:         This function create req for validating login form
             * Note:
             ********************************************************************/
            $scope.Mainbutton = function(){
                var address;
                var bantime = new Date();
                address = userInfo.ServerIP + userInfo.ServerPort + httpGet.getAccauntInfo; // host + req string
                conf.login = $scope.inputtextLogin;
                conf.password = $scope.inputtextPassword;
                $http({
                    method: 'PUT',
                    url: address,
                    data: conf,
                }).then(function(data) {
                    var AccauntInfo = data.data;
                    //************if data received good - check pass and login in database***********
                    if (AccauntInfo != undefined)
                    {
                        if (AccauntInfo.Login == $scope.inputtextLogin && AccauntInfo.Password == $scope.inputtextPassword && AccauntInfo.Customer_ID != undefined)
                        {
                            userInfo.Customer_ID = AccauntInfo.Customer_ID;
                            $scope.text = "LOG IN!";
                            $scope.index = 0;
                            $scope.ngshowLogin = true;
                            $scope.ngshowlogapp = false;
                        }
                        if ($scope.ngshowLogin != true)
                        {
                            $scope.index++;
                            $scope.text = "INCORRECT LOGIN OR PASSWORD, TRY AGAIN!" + ' (' + $scope.index + ")";
                            if($scope.index > 3){
                                conf.ban = true;
                                conf.bantime = bantime.getTime();
                                getBanStatus($http, $scope, conf);
                                $scope.index = 0;
                                $scope.ngshowLogin = true;
                            }
                        }
                    }
                },function (data) {
                    console.log('Что-то пошло не так')
                });

            };
        }
    },function (err) {
        console.log('getBanStatus ERR')
    })



    // $http({
    //     method: "PUT",
    //     url: address,
    //     data: conf,
    // }).success(function(banStatus) {
    //      //************if data received good - check pass and login in database***********
    //     if (banStatus.banStatus == true)
    //     {
    //         conf.ban = true;
    //         conf.bantime = banStatus.banTime;
    //         $scope.text = "YOU ARE BANNED FOR " + conf.bantime + " minutes!";
    //         alert("Your are banned! Ban elapsed in "+ conf.bantime+" minutes.");
    //     }
    //     if (banStatus.banStatus == false)//if ban false --> send req for accInfo
    //     {
    //         /*********************************************************************
    //          * Function:         Mainbutton = function(){}
    //          * PreCondition:     myMush server started
    //          * Input:            Login, password
    //          * Return^           data or error
    //          * Overview:         This function create req for validating login form
    //          * Note:
    //          ********************************************************************/
    //         $scope.Mainbutton = function(){
    //             var address;
    //             var bantime = new Date();
    //             address = userInfo.ServerIP + userInfo.ServerPort + httpGet.getAccauntInfo; // host + req string
    //             conf.login = $scope.inputtextLogin;
    //             conf.password = $scope.inputtextPassword;
    //             $http({
    //                 method: 'PUT',
    //                 url: address,
    //                 data: conf,
    //             }).success(function(data) {
    //                 var AccauntInfo = data;
    //                 //************if data received good - check pass and login in database***********
    //                 if (AccauntInfo != undefined)
    //                 {
    //                         if (AccauntInfo.Login == $scope.inputtextLogin && AccauntInfo.Password == $scope.inputtextPassword && AccauntInfo.Customer_ID != undefined)
    //                         {
    //                             userInfo.Customer_ID = AccauntInfo.Customer_ID;
    //                             $scope.text = "LOG IN!";
    //                             $scope.index = 0;
    //                             $scope.ngshowLogin = true;
    //                             $scope.ngshowlogapp = false;
    //                         }
    //                     if ($scope.ngshowLogin != true)
    //                     {
    //                         $scope.index++;
    //                         $scope.text = "INCORRECT LOGIN OR PASSWORD, TRY AGAIN!" + ' (' + $scope.index + ")";
    //                         if($scope.index > 3){
    //                             conf.ban = true;
    //                             conf.bantime = bantime.getTime();
    //                             getBanStatus($http, $scope, conf);
    //                             $scope.index = 0;
    //                             $scope.ngshowLogin = true;
    //                         }
    //                     }
    //                 }
    //             }).error(function (err) {
    //                 console.log('Что-то пошло не так')
    //             })
    //
    //         };
    //     }
    // }).error(function (err) {
    //     console.log('getBanStatus ERR')
    // })

}