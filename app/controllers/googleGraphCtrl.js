

function graphDyn($scope, $http){

    if ($scope.globalVars.language == "English"){
        var graphTags = [];
        graphTags['myButtonMarkers'] = 'Points';
        graphTags['myButtonValues'] = 'Values';
        graphTags['xaxisTitle'] = 'Time';
        graphTags['yaxisTitlePrim'] = 'Hudmunity';
        graphTags['yaxisTitleSec'] = 'Temperature';
        graphTags['rangeSelector5Min'] = '5 m';
        graphTags['rangeSelector15Min'] = '15 m';
        graphTags['rangeSelector30Min'] = '30 m';
        graphTags['rangeSelector3Hour'] = '3 h';
        graphTags['rangeSelector12Hour'] = '12 h';
        graphTags['rangeSelector24Hour'] = '24 h';
        graphTags['rangeSelector3Days'] = '3 d';
        graphTags['rangeSelectorAll'] = 'all';
    }
    if ($scope.globalVars.language == "Russian"){
        var graphTags = [];
        graphTags['myButtonMarkers'] = 'Точки';
        graphTags['myButtonValues'] = 'Значения';
        graphTags['xaxisTitle'] = 'Время';
        graphTags['yaxisTitlePrim'] = 'Влажность';
        graphTags['yaxisTitleSec'] = 'Температура';
        graphTags['rangeSelector5Min'] = '5 м';
        graphTags['rangeSelector15Min'] = '15 м';
        graphTags['rangeSelector30Min'] = '30 м';
        graphTags['rangeSelector3Hour'] = '3 ч';
        graphTags['rangeSelector12Hour'] = '12 ч';
        graphTags['rangeSelector24Hour'] = '24 ч';
        graphTags['rangeSelector3Days'] = '3 д';
        graphTags['rangeSelectorAll'] = 'всё';
    }

    $scope.chart = {};//MAIN MENU button flags arr
    $scope.chart["HudmReady"] = true;
    $scope.chart["HudmLoadBar"] = false;
    $scope.chart["HudmLoadBarVal"] = 0;

    // hudmunity loading bar
    // if ($scope.$$prevSibling == null){
    //     $scope.bar1 = new ldBar("#myItem2");
    //     $scope.bar2 = document.getElementById('myItem2').ldBar;
    //     $scope.bar1.set(0);
    // }

    $(document).ready(function () {
        Highcharts.setOptions({
            global: {
                useUTC: false,
            },
            lang: {
                myButtonMarkers: graphTags.myButtonMarkers,
                myButtonValues: graphTags.myButtonValues,
            },
        });

        var chart = Highcharts.stockChart('chart_div1', {
                chart: {
                    events: {
                    },
                    spacingRight: 30,
                    spacingLeft: 10,
                },
                tooltip: {
                    formatter: function() {
                        var s = [];
                        $.each(this.points, function(i, point) {
                            var dateTime = new Date(point.x);
                            var date = dateTime.toDateString();
                            var time = dateTime.toLocaleTimeString();
                            s.push(point.series.name + '<br>' +  point.y +'%' + '<br>' + date + '<br>' + time + '<br>')
                        });
                        return s;
                    },
                },
                xAxis:{
                    lineColor:'#999',
                    lineWidth:1,
                    tickColor:'#666',
                    tickLength:3,
                    type: 'datetime',
                    tickPixelInterval: 100,
                    // tickInterval: 20,
                    // showLastLabel: true,
                    // endOnTick: true,
                    // startOnTick: true,
                    title:{
                        text: graphTags.xaxisTitle,
                    },
                    labels: {
                        step: 0.5,
                    }
                    // minRange: 3600*100 // one hour
                },
                yAxis:[
                    {
                        opposite: true,
                        lineColor:'#999',
                        lineWidth:1,
                        tickColor:'#666',
                        tickWidth:1,
                        tickLength:3,
                        gridLineColor:'#ddd',
                        min: 20,
                        max: 100,
                        tickInterval: 20,
                        title:{
                            x: 15,
                            text: graphTags.yaxisTitlePrim,
                            rotation: -90,
                            margin: 30,
                            style: {
                                fontSize: 16,
                            }
                        },
                        labels: {
                            x: 25,
                            enabled: true,
                            step: 0.5,
                            format: '{value}%',
                            style: {
                                color: Highcharts.getOptions().colors[0],
                            }
                        },
                    },
                    { // Secondary yAxis
                        min: 0,
                        max: 100,
                        tickInterval: 10,
                        title: {
                            text: graphTags.yaxisTitleSec,
                            style: {
                                color: Highcharts.getOptions().colors[0],
                                fontSize: 16,
                            }
                        },
                        labels: {
                            step: 0.5,
                            format: '{value}°C',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: false
                    },
                ],
                rangeSelector: {
                    buttons: [{
                        count: 5,
                        type: 'minute',
                        text: graphTags.rangeSelector5Min,
                    }, {
                        count: 15,
                        type: 'minute',
                        text: graphTags.rangeSelector15Min,
                    },{
                        count: 30,
                        type: 'minute',
                        text: graphTags.rangeSelector30Min,
                    },{
                        count: 3,
                        type: 'hour',
                        text: graphTags.rangeSelector3Hour,
                    },{
                        count: 12,
                        type: 'hour',
                        text: graphTags.rangeSelector12Hour,
                    },{
                        count: 24,
                        type: 'hour',
                        text: graphTags.rangeSelector24Hour,
                    },{
                        count: 168,
                        type: 'hour',
                        text: graphTags.rangeSelector3Days,
                    }, {
                        type: 'all',
                        text: graphTags.rangeSelectorAll,
                    },],
                    inputEnabled: false,
                    selected: 8,
                },

                title: {
                    text: ''
                },

                exporting: {
                    buttons: {
                        'showMarkers': {
                            _id: 'showMarkers',
                            symbol: 'diamond',
                            _titleKey: "myButtonMarkers",
                            x: -62,
                            symbolFill: '#B5C9DF',
                            hoverSymbolFill: '#779ABF',
                            onclick: function () {
                                for (i=0; i<chart.series.length - 1; i++){
                                    if (chart.series[i].options.marker.enabled == false){
                                        for (k=0; k<chart.series.length - 1; k++){
                                            chart.series[k].update({
                                                marker: {
                                                    enabled: true,
                                                }
                                            })
                                        }
                                        $scope.globalVars.chart.showMarkers = true;
                                        return;
                                    }
                                    else{
                                        for (k=0; k<chart.series.length - 1; k++){
                                            chart.series[k].update({
                                                marker: {
                                                    enabled: false,
                                                }
                                            })
                                        }
                                        $scope.globalVars.chart.showMarkers = false;
                                        return;
                                    }
                                }
                            },
                        },
                        'showValues': {
                            _id: 'showValues',
                            symbol: 'circle',
                            _titleKey: "myButtonValues",
                            x: -92,
                            symbolFill: '#B5C9DF',
                            hoverSymbolFill: '#779ABF',
                            onclick: function () {
                                for (i=0; i<chart.series.length - 1; i++){
                                    if (chart.series[i].options.dataLabels.enabled == false){
                                        for (k=0; k<chart.series.length - 1; k++){
                                            chart.series[k].update({
                                                dataLabels: {
                                                    enabled: true,
                                                }
                                            })
                                        }
                                        $scope.globalVars.chart.showValues = true;
                                        return;
                                    }
                                    else{
                                        for (k=0; k<chart.series.length - 1; k++){
                                            chart.series[k].update({
                                                dataLabels: {
                                                    enabled: false,
                                                }
                                            })
                                        }
                                        $scope.globalVars.chart.showValues = false;
                                        return;
                                    }
                                }
                            },
                        },
                    },
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    spline: {
                        turboThreshold: 0,
                        lineWidth: 2,
                        states: {
                            hover: {
                                enabled: true,
                                lineWidth: 3
                            }
                        },
                        marker: {
                            enabled: false,
                            states: {
                                hover: {
                                    enabled : true,
                                    radius: 6,
                                    lineWidth: 1
                                }
                            }
                        },
                    },
                    line: {
                        dataLabels: {
                            enabled: true,
                            // formatter: function() {
                            //     if(this.point.index == 0) return '';
                            //     return this.y +'mm';
                            // }
                        }
                    },
                    series:{
                        // yAxis: 1,
                        turboThreshold: 1000000,//set it to a larger threshold, it is by default to 1000
                        dataLabels: {
                            enabled: false,
                        },
                    },
                    area: {
                        lineWidth: 1,
                    },
                },
            });
        // var chart = $scope.globalVars.chart;

        setInterval(function () { // update hudmunity data
            $scope.frozeHudmGraph = false;
            if ($scope.frozeHudmGraph == false)
            {
                var temp=0;
                // $scope.ClickSensorsBtn = $scope.$parent.$$childTail.$$prevSibling.ClickSensorsBtn;
                $scope.ClickSensorsBtn = $scope.$parent.globalVars.ClickSensorsBtn;
                for (var key in $scope.ClickSensorsBtn) {
                    if ($scope.ClickSensorsBtn[key] == true && key.substring(0,4) == "Hudm"){
                        var uploadTerm = key;
                    }
                }
                var address;
                var conf={// http req param
                    Customer_ID: "",
                    LastID: uploadTerm,
                };
                address = userInfo.ServerIP + userInfo.ServerPort + httpGet.getHudmChartData; // host + req string
                conf.Customer_ID = userInfo.Customer_ID;
                $http({
                    method: 'PUT',
                    url: address,
                    data: conf,
                }).then(function(sensors) {
                    //************if data received good - check pass and login in database***********
                    var sensors = sensors.data;

                    if (sensors != undefined && sensors.length != 0 && sensors[0].Master_ID != undefined)
                    {
                        $scope.getChartData = sensors;
                        $scope.getHudmChartData = sensors;
                        $scope.globalVars.sensorDataType = "H";
                        formatDataChartTest($scope, chart);
                        // formatDataChartHudm($scope, chart);
                    }
                }),function (err) {
                    console.log('getHudmChartData ERR');
                    $scope.serverConnected = false;
                    console.log(err);
                };
            }
        },2000, $scope, chart);

        setInterval(function () {       // update temperature data
            $scope.frozeTempGraph = false;
            if ($scope.frozeTempGraph == false)
            {
                var temp=0;
                // $scope.ClickSensorsBtn = $scope.$parent.$$childTail.$$prevSibling.ClickSensorsBtn;
                // $scope.ClickSensorsBtn = $scope.$parent.globalVars.ClickSensorsBtn;
                for (var key in $scope.globalVars.ClickSensorsBtn) {
                    if ($scope.globalVars.ClickSensorsBtn[key] == true && key.substring(0,4) == "Temp"){
                        var uploadTerm = key;
                    }
                }
                var address;
                var conf={// http req param
                    Customer_ID: "",
                    LastID: uploadTerm,
                };
                address = userInfo.ServerIP + userInfo.ServerPort + httpGet.getTempChartData; // host + req string
                conf.Customer_ID = userInfo.Customer_ID;
                $http({
                    method: 'PUT',
                    url: address,
                    data: conf,
                }).then(function(sensors) {
                    //************if data received good - check pass and login in database***********
                    var sensors = sensors.data;

                    if (sensors != undefined && sensors.length != 0 && sensors[0].Master_ID != undefined)
                    {
                        $scope.getChartData = sensors;
                        $scope.getTempChartData = sensors;
                        $scope.globalVars.sensorDataType = "ds18b20";
                        formatDataChartTest($scope, chart);
                        // formatDataChartTemp($scope, chart);
                    }
                }),function (err) {
                    console.log('getTempChartData ERR');
                    $scope.serverConnected = false;
                    console.log(err);
                };
            }
        },2000, $scope, chart);
    });
};














function formatDataChartTest($scope, chart){//create google Data Table from Mysql Data
    var chart = chart;
    var sensors = Array();
    var sensorCount = 0;
    var temp = Array();

    //******************************construct columns***********************************************************
    // $scope.globalVars.sensorsFinal = $scope.$$prevSibling.sensorsFinal;
    if ($scope.globalVars.sensorsFinal == undefined || $scope.globalVars.sensorsFinal == "" || $scope.globalVars.sensorsFinal == null)
    {
        return;
    }
    for(i=0;i<$scope.globalVars.sensorsFinal.length;i++){
        temp.push({});
        for (var key in $scope.globalVars.sensorsFinal[i]) {//change all menu's buttons flags to false
            if (key != "$$hashKey")
            {
                temp[i][key] = $scope.globalVars.sensorsFinal[i][key];
            }
        }
    }

    if (chart.series == undefined)//add column first time
    {
        return;
    }
    if (chart.series.length == 0)//add column first time
    {
        var firstGraphLoad = true;
    }
    else{
        var firstGraphLoad = false;
    }
    for(i=0;i<temp.length;i++)//add all temperature sensors  data to Hudm graph
    {
        temp[i].Date = temp[i].Date.split("-");
        temp[i].Time = temp[i].Time.split(":");
        if(temp[i].Type == "ds18b20" || temp[i].Type == "H"){
            sensors.push(temp[i]);
            sensorCount++;
            if (firstGraphLoad == true)//add column first time
            {
                chart.addSeries({
                    name: temp[i].Name,
                });
            }
        }
    }

    //******************if number of sensors was changed. Update Chart's DataTable structure
    if(firstGraphLoad == false) {
        var columnCoef = sensorCount + 1;
        if (columnCoef == chart.series.length){//if number of sensors is not changed from last data update
            for(i=0;i<sensorCount;i++){
                if (sensors[i].Name == chart.series[i].name){//if name of sensor is not changed from last data update
                }
                else{//if name of sensor was changed  - we need update name in column
                    chart.series[i].name = sensors[i].Name;
                    chart.legend.allItems[i].update({name: sensors[i].Name});
                    chart.rangeSelector.update({selected: chart.rangeSelector.selected});
                }
            }
        }

        if (columnCoef > chart.series.length){//if number of sensors greater than columns
            var sensorCountOld = chart.series.length-1;
            var addNewColumn = sensorCount - sensorCountOld;
            for (j=0; j<addNewColumn; j++){
                chart.addSeries({
                    name: 'undefined' + j,
                });
                for (k=1; k<chart.series.length - 1; k++){
                    // var color = "";
                    // switch (k)//get series color
                    // {
                    //     case 1 ://
                    //         color = '#ffc0cb';
                    //         break
                    //     case 2 ://
                    //         color = '#008080';
                    //         break
                    //     case 3 ://
                    //         color = '#ccff00';
                    //         break
                    //     case 4 ://
                    //         color = '#ff0000';
                    //         break
                    //     case 5: //
                    //         color = '#ffd700';
                    //         break
                    //     case 6: //
                    //         color = '#00ffff';
                    //         break
                    //     case 7 ://
                    //         color = '#ff7373';
                    //         break
                    //     case 8 ://
                    //         color = '#0000ff';
                    //         break
                    //     case 9 ://
                    //         color = '#c0c0c0';
                    //         break
                    //     case 10 ://
                    //         color = '#003366';
                    //         break
                    //     case 11: //
                    //         color = '#800080';
                    //         break
                    //     case 12: //
                    //         color = '#00ff00';
                    //         break
                    //     case 13: //
                    //         color = '#666666';
                    //         break
                    //     case 14: //
                    //         color = '#c6e2ff';
                    //         break
                    //     case 15: //
                    //         color = '#fffF00';
                    //         break
                    //     case 16: //
                    //         color = '#468499';
                    //         break
                    //     case 17: //
                    //         color = '#fff68f';
                    //         break
                    //     case 18: //
                    //         color = '#800000';
                    //         break
                    //     case 19: //
                    //         color = '#008000';
                    //         break
                    //     case 20: //
                    //         color = '#ff00ff';
                    //         break
                    //     case 21 ://
                    //         color = '#c39797';
                    //         break
                    //     case 22: //
                    //         color = '#daa520';
                    //         break
                    //     case 23: //
                    //         color = '#0e2f44';
                    //         break
                    //     case 24: //
                    //         color = '#ff7f50';
                    //         break
                    //     case 25: //
                    //         color = '#ff4040';
                    //         break
                    //     case 26: //
                    //         color = '#66cccc';
                    //         break
                    //     case 27: //
                    //         color = '#a0db8e';
                    //         break
                    //     case 28: //
                    //         color = '#794044';
                    //         break
                    //     case 29: //
                    //         color = '#0099cc';
                    //         break
                    //     case 30: //
                    //         color = '#8a2be2';
                    //         break
                    //     case 31: //
                    //         color = '#ccff00';
                    //         break
                    //     default: break
                    // }
                    chart.series[k].update({
                        marker: {
                            enabled: chart.series[0].options.marker.enabled,
                        },
                        dataLabels: {
                            enabled: chart.series[0].options.dataLabels.enabled,
                        },
                        // color: color,
                    })
                }
            }
            for(i=0;i<sensorCount;i++){
                if (sensors[i].Name == chart.series[i].name){//if name of sensor is not changed from last data update
                }
                else{//if name of sensor was changed  - we need update name in column
                    chart.series[i].name = sensors[i].Name;
                    chart.legend.allItems[i].update({name: sensors[i].Name});
                    chart.series[k].update({
                        name: sensors[i].Name,
                    });
                }
            }
        }

        if (columnCoef < chart.series.length){//if quantity of sensors are less than columns
            var series = new Array();
            var counter = false;
            for(i=0; i<chart.series.length - 1; i++){
                for(j=0; j<sensorCount; j++) {
                    if (sensors[j].Name == chart.series[i].name) {//if name of sensor is not changed from last data update
                        counter = true;
                    }
                }
                if (counter == false){
                    chart.series[i].remove();
                }
                counter = false;
            }
        }
    }

    //**********************************************construct columns end***************************************


    //**********************************************delete not available sensors from log***************************************
    $scope.globalVars.sensorsCount = sensorCount;//number of online sensors
    var chartData = new Array();//filtred log data
    if ($scope.getChartData[0].Customer_ID != undefined){
        if (sensorCount != 0 || $scope.tempSensorCount != sensorCount){
            for (i=0; i<$scope.getChartData.length; i++){//filter log data by online sensors
                for (j=0; j<sensorCount; j++){
                    var jj = j;
                    var ii=i;
                    if($scope.getChartData[i].Customer_ID == sensors[j].Customer_ID && $scope.getChartData[i].Master_ID == sensors[j].Master_ID && $scope.getChartData[i].Slave_ID == sensors[j].Slave_ID && $scope.getChartData[i].Name == sensors[j].Name){
                        chartData.push($scope.getChartData[i]);
                    }
                }
            }
        }
        else{
            alert ("Sensors for GRAPH are not found!");
        }
    }

    //**************************************overflow protection (800000 points in series) *************************
    if ($scope.globalVars.dataSeries != ""){
        var dataSeries = $scope.globalVars.dataSeries;
    }
    else{
        var dataSeries = [];
    }
    // var dataSeries = [];

    for (i = 0; i < chart.series.length-1; i++){
        dataSeries.push([])
        if (chart.series[i].points.length != undefined){
            if (chart.series[i].points.length >= 80000){
                $scope.globalVars.ClickSensorsBtn.TempReq1Min = false;//change all processBar buttons flags to false
                $scope.globalVars.ClickSensorsBtn.TempBar1Hour = false;
                $scope.globalVars.ClickSensorsBtn.TempBar6Hour = false;
                $scope.globalVars.ClickSensorsBtn.TempBar1Day = false;
                $scope.globalVars.ClickSensorsBtn.TempBar1Week = false;
                $scope.globalVars.ClickSensorsBtn.TempBar3Month = false;
                for (var key in $scope.globalVars.ClickSensorsBtn) {
                    if (key == $scope.globalVars.processBar.hudm.lastPos)
                    {
                        $scope.globalVars.ClickSensorsBtn[key] = true;//set to true by last clicked
                        return;
                    }
                }
                $scope.globalVars.ClickSensorsBtn.HudmReq1Min = false;//change all processBar buttons flags to false
                $scope.globalVars.ClickSensorsBtn.HudmBar1Hour = false;
                $scope.globalVars.ClickSensorsBtn.HudmBar6Hour = false;
                $scope.globalVars.ClickSensorsBtn.HudmBar1Day = false;
                $scope.globalVars.ClickSensorsBtn.HudmBar1Week = false;
                $scope.globalVars.ClickSensorsBtn.HudmBar3Month = false;
                for (var key in $scope.globalVars.ClickSensorsBtn) {
                    if (key == $scope.globalVars.processBar.temp.lastPos)
                    {
                        $scope.globalVars.ClickSensorsBtn[key] = true;//set to true by last clicked
                        return;
                    }
                }
            }
        }
    }
    //**************************************end*************************************************************************
    //**************************************restore graph settings******************************************************
    for (i=0; i<chart.series.length - 1; i++){ // restore show values
        if (chart.series[i].options.dataLabels.enabled == false && $scope.globalVars.chart.showValues){
            for (k=0; k<chart.series.length - 1; k++){
                chart.series[k].update({
                    dataLabels: {
                        enabled: true,
                    }
                })
            }
        }
    }
    for (i=0; i<chart.series.length - 1; i++){ // restore show markers
        if (chart.series[i].options.marker.enabled == false && $scope.globalVars.chart.showMarkers){
            for (k=0; k<chart.series.length - 1; k++){
                chart.series[k].update({
                    marker: {
                        enabled: true,
                    }
                })
            }
        }
    }
    //**************************************end*************************************************************************


    //**************************************update Temperature Series*************************************************************************
    if ($scope.tempDataStatus == 0 && $scope.globalVars.ClickSensorsBtn.TempReq1Min == false && $scope.globalVars.sensorDataType == "ds18b20"){
        for (k = 0; k < chart.series.length - 1; k++) {
            var dataSeriesFlag = false;
            for (i=0; i < chartData.length; i++) {
                if (chartData[i].Name == chart.series[k].name) {
                    var x = new Date(chartData[i].Date[0], chartData[i].Date[1]-1, chartData[i].Date[2], chartData[i].Time[0], chartData[i].Time[1], chartData[i].Time[2], 0).getTime();
                    if (dataSeries[k].length != 0){
                        var cnt = dataSeries[k].length - 1;
                        if (x == dataSeries[k][cnt].x){
                            x++;
                        }
                    }
                    dataSeries[k].push({
                        name: chart.series[k].name,
                        x: x,
                        y: chartData[i].Value,
                    });
                    dataSeriesFlag = true;
                    var loadingBar = (k * 100) / chartData.length;
                    if (loadingBar > 100) {
                        loadingBar = 100;
                    }
                    if (loadingBar < 0) {
                        loadingBar = 0;
                    }
                    // $scope.$$prevSibling.bar1.set(loadingBar);
                }
            }
            if (dataSeriesFlag == true){
                chart.series[k].update({
                    data: dataSeries[k],
                    yAxis: 1,
                });
            }
        }
        $scope.globalVars.dataSeries = dataSeries;
        $scope.globalVars.ClickSensorsBtn["TempReq1Min"] = true;// get TempChartData of 1min
        $scope.globalVars.ClickSensorsBtn["TempBar1Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar6Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Day"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Week"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar3Month"] = false;

        $scope.tempDataStatus = 1;//first preload completed
        return;
    }


    if ($scope.tempDataStatus == 1 && $scope.globalVars.ClickSensorsBtn.TempReq1Min == true && $scope.globalVars.sensorDataType == "ds18b20"){
        dataSeries = [];
        var count = 0;
        var k = "";
        var i = 0;
        for (k = 0; k < chartData.length; k++) {
            for (i=0; i < chart.series.length - 1; i++) {
                $scope.tempI++;
                if (chartData[k].Name == chart.series[i].name) {
                    var x = new Date(chartData[k].Date[0], chartData[k].Date[1]-1, chartData[k].Date[2], chartData[k].Time[0], chartData[k].Time[1], chartData[k].Time[2], 0).getTime();
                    var index1 = i;
                    var index2 = k;
                    i;
                    k;
                    var pointsLength = chart.series[i].points.length;
                    var xDataLength = chart.series[i].xData.length;
                    if (xDataLength == 0) {
                        chart.series[i].addPoint([x, chartData[k].Value], false, false);
                        chart.series[i].update({
                            yAxis: 1,
                        });
                        // chart.series[0].addPoint([,], true, false);
                    }
                    else {
                        if (chart.series[i].xData[xDataLength - 1] < x) {
                            count++;
                            chart.series[i].addPoint([x, chartData[k].Value], false, false);
                            if (i != 0 || chartData.length < 80) {
                                chart.series[0].addPoint([,], true, false);
                            }
                        }
                        if (count > 10) {
                            // $scope.globalVars.TempCount = k;
                            // $scope.chart.HudmLoadBar = true;
                            return;
                        }
                        else {
                            $scope.chart.HudmLoadBarVal++;
                            var loadingBar = ($scope.chart.HudmLoadBarVal * 100) / chartData.length;
                            if (loadingBar > 100) {
                                loadingBar = 100;
                            }
                            if (loadingBar < 0) {
                                loadingBar = 0;
                            }
                            // $scope.$$prevSibling.bar1.set(loadingBar);
                        }
                    }
                }
            }
        }
    }
    //**************************************end*************************************************************************

    //**************************************update Hudmunity Series*************************************************************************
    if ($scope.HudmDataStatus == 0 && $scope.globalVars.ClickSensorsBtn.HudmReq1Min == false && $scope.globalVars.sensorDataType == "H"){
        var dataSeriesFlag = false;
        for (k = 0; k < chart.series.length - 1; k++) {
            for (i=0; i < chartData.length; i++) {
                if (chartData[i].Name == chart.series[k].name) {
                    var x = new Date(chartData[i].Date[0], chartData[i].Date[1]-1, chartData[i].Date[2], chartData[i].Time[0], chartData[i].Time[1], chartData[i].Time[2], 0).getTime();
                    if (dataSeries[k].length != 0){
                        var cnt = dataSeries[k].length - 1;
                        if (x == dataSeries[k][cnt].x){
                            x++;
                        }
                    }
                    dataSeries[k].push({
                        name: chart.series[k].name,
                        x: x,
                        y: chartData[i].Value,
                    });
                    dataSeriesFlag = true;
                    var loadingBar = (k * 100) / chartData.length;
                    if (loadingBar > 100) {
                        loadingBar = 100;
                    }
                    if (loadingBar < 0) {
                        loadingBar = 0;
                    }
                    // $scope.$$prevSibling.bar1.set(loadingBar);
                }
            }
            if (dataSeriesFlag){
                chart.series[k].update({
                    data: dataSeries[k],
                });
            }
        }
        $scope.globalVars.dataSeries = dataSeries;
        $scope.globalVars.ClickSensorsBtn["HudmReq1Min"] = true;
        $scope.globalVars.ClickSensorsBtn["HudmBar1Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar6Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar1Day"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar1Week"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar3Month"] = false;

        // chart.rangeSelector.update({selected: 8});
        $scope.HudmDataStatus = 1;//first preload completed
        return;
    }


    if ($scope.HudmDataStatus == 1 && $scope.globalVars.ClickSensorsBtn.HudmReq1Min == true && $scope.globalVars.sensorDataType == "H"){
        dataSeries = [];
        var count = 0;
        var k = "";
        var i = 0;
        for (k = 0; k < chartData.length; k++) {
            for (i=0; i < chart.series.length - 1; i++) {
                $scope.tempI++;
                if (chartData[k].Name == chart.series[i].name) {
                    var x = new Date(chartData[k].Date[0], chartData[k].Date[1]-1, chartData[k].Date[2], chartData[k].Time[0], chartData[k].Time[1], chartData[k].Time[2], 0).getTime();
                    var index1 = i;
                    var index2 = k;
                    i;
                    k;
                    var pointsLength = chart.series[i].points.length;
                    var xDataLength = chart.series[i].xData.length;
                    if (xDataLength == 0) {
                        chart.series[i].addPoint([x, chartData[k].Value], true, false);
                        chart.series[i].update({
                            yAxis: 0,
                        });
                        // chart.series[0].addPoint([,], true, false);
                    }
                    else {
                        if (chart.series[i].xData[xDataLength - 1] < x) {
                            count++;
                            chart.series[i].addPoint([x, chartData[k].Value], true, false);
                            if (i != 0 || chartData.length < 80) {
                                chart.series[0].addPoint([,], true, false);
                                // chart.series[0].addPoint([,]);
                                // if (chart.series[i].options.yAxis == undefined  || chart.series[i].options.yAxis != 0){
                                //     chart.series[i].update({
                                //         yAxis: 0,
                                //     });
                                // }
                            }
                        }
                        if (count > 10) {
                            // $scope.globalVars.TempCount = k;
                            // $scope.chart.HudmLoadBar = true;
                            return;
                        }
                        else {
                            $scope.chart.HudmLoadBarVal++;
                            var loadingBar = ($scope.chart.HudmLoadBarVal * 100) / chartData.length;
                            if (loadingBar > 100) {
                                loadingBar = 100;
                            }
                            if (loadingBar < 0) {
                                loadingBar = 0;
                            }
                            // $scope.$$prevSibling.bar1.set(loadingBar);
                        }
                    }
                }
            }
        }
    }
    //**************************************end*************************************************************************
}













function formatDataChartHudm($scope, chart){//create google Data Table from Mysql Data
    var chart = chart;
    var hudmSensors = Array();
    var HudmSensorCount = 0;
    var tempHudm = Array();

    //******************************construct columns***********************************************************
    // $scope.globalVars.sensorsFinal = $scope.$$prevSibling.sensorsFinal;
    if ($scope.globalVars.sensorsFinal == undefined || $scope.globalVars.sensorsFinal == "" || $scope.globalVars.sensorsFinal == null)
    {
        return;
    }
    for(i=0;i<$scope.globalVars.sensorsFinal.length;i++){
        tempHudm.push({});
        for (var key in $scope.globalVars.sensorsFinal[i]) {//change all menu's buttons flags to false
            if (key != "$$hashKey")
            {
                tempHudm[i][key] = $scope.globalVars.sensorsFinal[i][key];
            }
        }
    }
    // tempHudm.splice(1);
    if (chart.series == undefined)//add column first time
    {
        return;
    }
    if (chart.series.length == 0)//add column first time
    {
        var firstGraphLoad = true;
    }
    else{
        var firstGraphLoad = false;
    }
    for(i=0;i<tempHudm.length;i++)//add all hudmunity sensors  data to Hudm graph
    {
        tempHudm[i].Date = tempHudm[i].Date.split("-");
        tempHudm[i].Time = tempHudm[i].Time.split(":");
        if(tempHudm[i].Type == "H"){
            hudmSensors.push(tempHudm[i]);
            HudmSensorCount++;
            if (firstGraphLoad == true)//add column first time
            {
                chart.addSeries({
                    name: tempHudm[i].Name,
                });
            }
        }
    }

    //******************if number of sensors was changed. Update Chart's DataTable structure
    if(firstGraphLoad == false) {
        var columnCoef = HudmSensorCount + 1;
        if (columnCoef == chart.series.length){//if number of sensors is not changed from last data update
            for(i=0;i<HudmSensorCount;i++){
                if (hudmSensors[i].Name == chart.series[i].name){//if name of sensor is not changed from last data update
                }
                else{//if name of sensor was changed  - we need update name in column
                    chart.series[i].name = hudmSensors[i].Name;
                    chart.legend.allItems[i].update({name: hudmSensors[i].Name});
                    chart.rangeSelector.update({selected: chart.rangeSelector.selected});
                }
            }
        }

        if (columnCoef > chart.series.length){//if number of sensors greater than columns
            var HudmSensorCountOld = chart.series.length-1;
            var addNewColumn = HudmSensorCount - HudmSensorCountOld;
            for (j=0; j<addNewColumn; j++){
                chart.addSeries({
                    name: 'undefined' + j,
                });
                for (k=1; k<chart.series.length - 1; k++){
                    // var color = "";
                    // switch (k)//get series color
                    // {
                    //     case 1 ://
                    //         color = '#ffc0cb';
                    //         break
                    //     case 2 ://
                    //         color = '#008080';
                    //         break
                    //     case 3 ://
                    //         color = '#ccff00';
                    //         break
                    //     case 4 ://
                    //         color = '#ff0000';
                    //         break
                    //     case 5: //
                    //         color = '#ffd700';
                    //         break
                    //     case 6: //
                    //         color = '#00ffff';
                    //         break
                    //     case 7 ://
                    //         color = '#ff7373';
                    //         break
                    //     case 8 ://
                    //         color = '#0000ff';
                    //         break
                    //     case 9 ://
                    //         color = '#c0c0c0';
                    //         break
                    //     case 10 ://
                    //         color = '#003366';
                    //         break
                    //     case 11: //
                    //         color = '#800080';
                    //         break
                    //     case 12: //
                    //         color = '#00ff00';
                    //         break
                    //     case 13: //
                    //         color = '#666666';
                    //         break
                    //     case 14: //
                    //         color = '#c6e2ff';
                    //         break
                    //     case 15: //
                    //         color = '#fffF00';
                    //         break
                    //     case 16: //
                    //         color = '#468499';
                    //         break
                    //     case 17: //
                    //         color = '#fff68f';
                    //         break
                    //     case 18: //
                    //         color = '#800000';
                    //         break
                    //     case 19: //
                    //         color = '#008000';
                    //         break
                    //     case 20: //
                    //         color = '#ff00ff';
                    //         break
                    //     case 21 ://
                    //         color = '#c39797';
                    //         break
                    //     case 22: //
                    //         color = '#daa520';
                    //         break
                    //     case 23: //
                    //         color = '#0e2f44';
                    //         break
                    //     case 24: //
                    //         color = '#ff7f50';
                    //         break
                    //     case 25: //
                    //         color = '#ff4040';
                    //         break
                    //     case 26: //
                    //         color = '#66cccc';
                    //         break
                    //     case 27: //
                    //         color = '#a0db8e';
                    //         break
                    //     case 28: //
                    //         color = '#794044';
                    //         break
                    //     case 29: //
                    //         color = '#0099cc';
                    //         break
                    //     case 30: //
                    //         color = '#8a2be2';
                    //         break
                    //     case 31: //
                    //         color = '#ccff00';
                    //         break
                    //     default: break
                    // }
                    chart.series[k].update({
                        marker: {
                            enabled: chart.series[0].options.marker.enabled,
                        },
                        dataLabels: {
                            enabled: chart.series[0].options.dataLabels.enabled,
                        },
                        // color: color,
                    })
                }
            }
            for(i=0;i<HudmSensorCount;i++){
                if (hudmSensors[i].Name == chart.series[i].name){//if name of sensor is not changed from last data update
                }
                else{//if name of sensor was changed  - we need update name in column
                    chart.series[i].name = hudmSensors[i].Name;
                    chart.legend.allItems[i].update({name: hudmSensors[i].Name});
                    $scope.HudmDataStatus = 0;//first preload completed
                    $scope.ClickSensorsBtn.HudmReq1Min = false;
                    for (var key0 in $scope.$parent.SensorBtnLastPassive){
                        for (var key in $scope.ClickSensorsBtn) {//change all menu's buttons flags to false
                            if (key == key0)
                            {
                                $scope.ClickSensorsBtn[key] = $scope.$parent.SensorBtnLastPassive[key0];
                                return;
                            }
                        }
                    }
                }
            }
        }

        if (columnCoef < chart.series.length){//if number of sensors less than columns
            var tempSeries = new Array();
            var counter = false;
            for(i=0; i<chart.series.length - 1; i++){
                for(j=0; j<HudmSensorCount; j++) {
                    if (hudmSensors[j].Name == chart.series[i].name) {//if name of sensor is not changed from last data update
                        counter = true;
                    }
                }
                if (counter == false){
                    chart.series[i].remove();
                }
                counter = false;
            }
        }
    }

    //**********************************************construct columns end***************************************



    //**********************************************delete not available sensors from log***************************************
    $scope.getHudmChartData;
    $scope.HudmSensorCount = HudmSensorCount;//number of online Hudm sensors
    var chartData = new Array();//filtred log data
    if ($scope.getHudmChartData[0].Customer_ID != undefined){
        if ($scope.HudmSensorCount != 0 || $scope.HudmSensorCount != undefined){
            for (i=0; i<$scope.getHudmChartData.length; i++){//filter log data by online sensors
                for (j=0; j<HudmSensorCount; j++){
                    var jj = j;
                    var ii=i;
                    if($scope.getHudmChartData[i].Customer_ID == hudmSensors[j].Customer_ID && $scope.getHudmChartData[i].Master_ID == hudmSensors[j].Master_ID && $scope.getHudmChartData[i].Slave_ID == hudmSensors[j].Slave_ID && $scope.getHudmChartData[i].Name == hudmSensors[j].Name){
                        chartData.push($scope.getHudmChartData[i]);
                    }
                }
            }
        }
        else{
            alert ("Hudmunity sensors not found!");
        }
    }
    else{
        // alert ("Current log request is empty!");
    }

    var data_hash = [];
    var dataSeries = [];
    for (i = 0; i < chart.series.length-1; i++){
        dataSeries.push([])
        if (chart.series[i].points.length >= 800000){
            $scope.ClickSensorsBtn.HudmReq1Min = false;
            for (var key0 in $scope.$parent.SensorBtnLastPassive){
                for (var key in $scope.ClickSensorsBtn) {//change all menu's buttons flags to false
                    if (key == key0)
                    {
                        $scope.ClickSensorsBtn[key] = $scope.$parent.SensorBtnLastPassive[key0];
                        // chart.series[i].setData([]);
                        return;
                    }
                }
            }
        }
    }

    if ($scope.HudmDataStatus == 0 && $scope.ClickSensorsBtn.HudmReq1Min == false){
        for (k = 0; k < chart.series.length - 1; k++) {
            for (i=0; i < chartData.length; i++) {
                if (chartData[i].Name == chart.series[k].name) {
                    var x = new Date(chartData[i].Date[0], chartData[i].Date[1]-1, chartData[i].Date[2], chartData[i].Time[0], chartData[i].Time[1], chartData[i].Time[2], 0).getTime();
                    if (dataSeries[k].length != 0){
                        var cnt = dataSeries[k].length - 1;
                        if (x == dataSeries[k][cnt].x){
                            x++;
                        }
                    }
                    dataSeries[k].push({
                        name: chart.series[k].name,
                        x: x,
                        y: chartData[i].Value,
                    });
                    var loadingBar = (k * 100) / chartData.length;
                    if (loadingBar > 100) {
                        loadingBar = 100;
                    }
                    if (loadingBar < 0) {
                        loadingBar = 0;
                    }
                    // $scope.$$prevSibling.bar1.set(loadingBar);
                }
            }
            chart.series[k].update({
                data: dataSeries[k],
            });
        }
        // $scope.$$prevSibling.tempI = 0;
        // $scope.$$prevSibling.HudmCount = 0;
        // $scope.$$prevSibling.chart.HudmLoadBarVal = 1;
        // $scope.$$prevSibling.chart.HudmReady = false;
        // $scope.$$prevSibling.chart.HudmLoadBar = true;
        $scope.tempI = 0;
        $scope.HudmCount = 0;
        $scope.chart.HudmLoadBarVal = 1;
        $scope.chart.HudmReady = false;
        $scope.chart.HudmLoadBar = true;

        $scope.ClickSensorsBtn["HudmBar1Hour"] = false;
        $scope.ClickSensorsBtn["HudmBar6Hour"] = false;
        $scope.ClickSensorsBtn["HudmBar1Day"] = false;
        $scope.ClickSensorsBtn["HudmBar1Week"] = false;
        $scope.ClickSensorsBtn["HudmBar3Month"] = false;
        $scope.ClickSensorsBtn["HudmReq1Min"] = true;

        $scope.HudmDataStatus = 1;//first preload completed
    }




    if ($scope.HudmDataStatus == 1 && $scope.ClickSensorsBtn.HudmReq1Min == true){
        var count = 0;
        // $scope.$$prevSibling.HudmCount;
        var k = "";
        var i = 0;
        for (k = $scope.HudmCount; k < chartData.length; k++) {
            for (i=0; i < chart.series.length - 1; i++) {
                $scope.tempI++;
                if (chartData[k].Name == chart.series[i].name) {
                    var x = new Date(chartData[k].Date[0], chartData[k].Date[1]-1, chartData[k].Date[2], chartData[k].Time[0], chartData[k].Time[1], chartData[k].Time[2], 0).getTime();
                    var index1 = i;
                    var index2 = k;
                    i;
                    k;
                    var pointsLength = chart.series[i].points.length;
                    var xDataLength = chart.series[i].xData.length;
                    if (xDataLength == 0) {
                        chart.series[i].addPoint([x, chartData[k].Value], true, false);
                    }
                    else {
                        if (chart.series[i].xData[xDataLength - 1] < x) {
                            count++;
                            chart.series[i].addPoint([x, chartData[k].Value], true, false);
                            if (i != 0 || chartData.length < 80) {
                                chart.series[0].addPoint([,], true, false);
                            }
                        }
                        if (count > 10) {
                            $scope.HudmCount = k;
                            $scope.chart.HudmLoadBar = true;
                            return;
                        }
                        else {
                            $scope.chart.HudmLoadBarVal++;
                            var loadingBar = ($scope.chart.HudmLoadBarVal * 100) / chartData.length;
                            if (loadingBar > 100) {
                                loadingBar = 100;
                            }
                            if (loadingBar < 0) {
                                loadingBar = 0;
                            }
                            // $scope.$$prevSibling.bar1.set(loadingBar);
                        }
                    }
                }
            }
        }
        // $scope.$$prevSibling.tempI = 0;
        // $scope.$$prevSibling.HudmCount = 0;
        // $scope.$$prevSibling.chart.HudmLoadBarVal = 1;
        // $scope.$$prevSibling.chart.HudmReady = false;
        // $scope.$$prevSibling.chart.HudmLoadBar = true;

        $scope.tempI = 0;
        $scope.HudmCount = 0;
        $scope.chart.HudmLoadBarVal = 1;
        $scope.chart.HudmReady = false;
        $scope.chart.HudmLoadBar = true;

        $scope.ClickSensorsBtn["HudmBar1Hour"] = false;
        $scope.ClickSensorsBtn["HudmBar6Hour"] = false;
        $scope.ClickSensorsBtn["HudmBar1Day"] = false;
        $scope.ClickSensorsBtn["HudmBar1Week"] = false;
        $scope.ClickSensorsBtn["HudmBar3Month"] = false;
        $scope.ClickSensorsBtn["HudmReq1Min"] = true;

        // $scope.HudmDataStatus = 1;//first preload completed
    }
}

function formatDataChartTemp($scope, chart){//create google Data Table from Mysql Data
    var chart = chart;
    var tempSensors = Array();
    var tempSensorCount = 0;
    var tempTemp = Array();

    //******************************construct columns***********************************************************
    // $scope.globalVars.sensorsFinal = $scope.$$prevSibling.sensorsFinal;
    if ($scope.globalVars.sensorsFinal == undefined || $scope.globalVars.sensorsFinal == "" || $scope.globalVars.sensorsFinal == null)
    {
        return;
    }
    for(i=0;i<$scope.globalVars.sensorsFinal.length;i++){
        tempTemp.push({});
        for (var key in $scope.globalVars.sensorsFinal[i]) {//change all menu's buttons flags to false
            if (key != "$$hashKey")
            {
                tempTemp[i][key] = $scope.globalVars.sensorsFinal[i][key];
            }
        }
    }

    if (chart.series == undefined)//add column first time
    {
        return;
    }
    if (chart.series.length == 0)//add column first time
    {
        var firstGraphLoad = true;
    }
    else{
        var firstGraphLoad = false;
    }
    for(i=0;i<tempTemp.length;i++)//add all temperature sensors  data to Hudm graph
    {
        tempTemp[i].Date = tempTemp[i].Date.split("-");
        tempTemp[i].Time = tempTemp[i].Time.split(":");
        if(tempTemp[i].Type == "ds18b20"){
            tempSensors.push(tempTemp[i]);
            tempSensorCount++;
            if (firstGraphLoad == true)//add column first time
            {
                chart.addSeries({
                    name: tempTemp[i].Name,
                });
            }
        }
    }

    //******************if number of sensors was changed. Update Chart's DataTable structure
    if(firstGraphLoad == false) {
        var columnCoef = tempSensorCount + 1;
        if (columnCoef == chart.series.length){//if number of sensors is not changed from last data update
            for(i=0;i<tempSensorCount;i++){
                if (tempSensors[i].Name == chart.series[i].name){//if name of sensor is not changed from last data update
                }
                else{//if name of sensor was changed  - we need update name in column
                    chart.series[i].name = tempSensors[i].Name;
                    chart.legend.allItems[i].update({name: tempSensors[i].Name});
                    chart.rangeSelector.update({selected: chart.rangeSelector.selected});
                }
            }
        }

        if (columnCoef > chart.series.length){//if number of sensors greater than columns
            var tempSensorCountOld = chart.series.length-1;
            var addNewColumn = tempSensorCount - tempSensorCountOld;
            for (j=0; j<addNewColumn; j++){
                chart.addSeries({
                    name: 'undefined' + j,
                });
                for (k=1; k<chart.series.length - 1; k++){
                    // var color = "";
                    // switch (k)//get series color
                    // {
                    //     case 1 ://
                    //         color = '#ffc0cb';
                    //         break
                    //     case 2 ://
                    //         color = '#008080';
                    //         break
                    //     case 3 ://
                    //         color = '#ccff00';
                    //         break
                    //     case 4 ://
                    //         color = '#ff0000';
                    //         break
                    //     case 5: //
                    //         color = '#ffd700';
                    //         break
                    //     case 6: //
                    //         color = '#00ffff';
                    //         break
                    //     case 7 ://
                    //         color = '#ff7373';
                    //         break
                    //     case 8 ://
                    //         color = '#0000ff';
                    //         break
                    //     case 9 ://
                    //         color = '#c0c0c0';
                    //         break
                    //     case 10 ://
                    //         color = '#003366';
                    //         break
                    //     case 11: //
                    //         color = '#800080';
                    //         break
                    //     case 12: //
                    //         color = '#00ff00';
                    //         break
                    //     case 13: //
                    //         color = '#666666';
                    //         break
                    //     case 14: //
                    //         color = '#c6e2ff';
                    //         break
                    //     case 15: //
                    //         color = '#fffF00';
                    //         break
                    //     case 16: //
                    //         color = '#468499';
                    //         break
                    //     case 17: //
                    //         color = '#fff68f';
                    //         break
                    //     case 18: //
                    //         color = '#800000';
                    //         break
                    //     case 19: //
                    //         color = '#008000';
                    //         break
                    //     case 20: //
                    //         color = '#ff00ff';
                    //         break
                    //     case 21 ://
                    //         color = '#c39797';
                    //         break
                    //     case 22: //
                    //         color = '#daa520';
                    //         break
                    //     case 23: //
                    //         color = '#0e2f44';
                    //         break
                    //     case 24: //
                    //         color = '#ff7f50';
                    //         break
                    //     case 25: //
                    //         color = '#ff4040';
                    //         break
                    //     case 26: //
                    //         color = '#66cccc';
                    //         break
                    //     case 27: //
                    //         color = '#a0db8e';
                    //         break
                    //     case 28: //
                    //         color = '#794044';
                    //         break
                    //     case 29: //
                    //         color = '#0099cc';
                    //         break
                    //     case 30: //
                    //         color = '#8a2be2';
                    //         break
                    //     case 31: //
                    //         color = '#ccff00';
                    //         break
                    //     default: break
                    // }
                    chart.series[k].update({
                        marker: {
                            enabled: chart.series[0].options.marker.enabled,
                        },
                        dataLabels: {
                            enabled: chart.series[0].options.dataLabels.enabled,
                        },
                        // color: color,
                    })
                }
            }
            for(i=0;i<tempSensorCount;i++){
                if (tempSensors[i].Name == chart.series[i].name){//if name of sensor is not changed from last data update
                }
                else{//if name of sensor was changed  - we need update name in column
                    chart.series[i].name = tempSensors[i].Name;
                    chart.legend.allItems[i].update({name: tempSensors[i].Name});
                    $scope.tempDataStatus = 0;//first preload completed
                    $scope.globalVars.ClickSensorsBtn.TempReq1Min = false;
                    for (var key in $scope.ClickSensorsBtn) {//change all menu's buttons flags to false
                        if (key == $scope.globalVars.processBar.temp.lastPos)
                        {
                            $scope.globalVars.ClickSensorsBtn[key] = true;
                            return;
                        }
                    }
                }
            }
        }

        if (columnCoef < chart.series.length){//if number of sensors less than columns
            var tempSeries = new Array();
            var counter = false;
            for(i=0; i<chart.series.length - 1; i++){
                for(j=0; j<tempSensorCount; j++) {
                    if (tempSensors[j].Name == chart.series[i].name) {//if name of sensor is not changed from last data update
                        counter = true;
                    }
                }
                if (counter == false){
                    chart.series[i].remove();
                }
                counter = false;
            }
        }
    }

    //**********************************************construct columns end***************************************



    //**********************************************delete not available sensors from log***************************************
    $scope.getTempChartData;
    $scope.tempSensorCount = tempSensorCount;//number of online Hudm sensors
    var chartData = new Array();//filtred log data
    if ($scope.getTempChartData[0].Customer_ID != undefined){
        if ($scope.tempSensorCount != 0 || $scope.tempSensorCount != undefined){
            for (i=0; i<$scope.getTempChartData.length; i++){//filter log data by online sensors
                for (j=0; j<tempSensorCount; j++){
                    var jj = j;
                    var ii=i;
                    if($scope.getTempChartData[i].Customer_ID == tempSensors[j].Customer_ID && $scope.getTempChartData[i].Master_ID == tempSensors[j].Master_ID && $scope.getTempChartData[i].Slave_ID == tempSensors[j].Slave_ID && $scope.getTempChartData[i].Name == tempSensors[j].Name){
                        chartData.push($scope.getTempChartData[i]);
                    }
                }
            }
        }
        else{
            alert ("ds18b20 sensors are not found!");
        }
    }
    else{
        // alert ("Current log request is empty!");
    }

    //**************************************overflow protection 800000 points Temperature graph*************************
    var data_hash = [];
    var dataSeries = $scope.globalVars.dataSeries;
    for (i = 0; i < chart.series.length-1; i++){
        dataSeries.push([])
        if (chart.series[i].points.length >= 800000){
            $scope.globalVars.ClickSensorsBtn.TempReq1Min = false;
            $scope.globalVars.ClickSensorsBtn.TempBar1Hour = false;
            $scope.globalVars.ClickSensorsBtn.TempBar6Hour = false;
            $scope.globalVars.ClickSensorsBtn.TempBar1Day = false;
            $scope.globalVars.ClickSensorsBtn.TempBar1Week = false;
            $scope.globalVars.ClickSensorsBtn.TempBar3Month = false;
            for (var key in $scope.ClickSensorsBtn) {//change all menu's buttons flags to false
                if (key == $scope.globalVars.processBar.temp.lastPos)
                {
                    $scope.globalVars.ClickSensorsBtn[key] = true;
                    return;
                }
            }
        }
    }
    //**************************************end*************************************************************************

    if ($scope.tempDataStatus == 0 && $scope.globalVars.ClickSensorsBtn.TempReq1Min == false){
        for (k = 0; k < chart.series.length - 1; k++) {
            for (i=0; i < chartData.length; i++) {
                if (chartData[i].Name == chart.series[k].name) {
                    var x = new Date(chartData[i].Date[0], chartData[i].Date[1]-1, chartData[i].Date[2], chartData[i].Time[0], chartData[i].Time[1], chartData[i].Time[2], 0).getTime();
                    if (dataSeries[k].length != 0){
                        var cnt = dataSeries[k].length - 1;
                        if (x == dataSeries[k][cnt].x){
                            x++;
                        }
                    }
                    dataSeries[k].push({
                        name: chart.series[k].name,
                        x: x,
                        y: chartData[i].Value,
                    });
                    var loadingBar = (k * 100) / chartData.length;
                    if (loadingBar > 100) {
                        loadingBar = 100;
                    }
                    if (loadingBar < 0) {
                        loadingBar = 0;
                    }
                    // $scope.$$prevSibling.bar1.set(loadingBar);
                }
            }
            chart.series[k].update({
                data: dataSeries[k],
                yAxis: 1,
            });
        }
        $scope.globalVars.dataSeries = dataSeries;
        $scope.globalVars.ClickSensorsBtn["TempReq1Min"] = true;// get TempChartData of 1min
        $scope.globalVars.ClickSensorsBtn["TempBar1Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar6Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Day"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Week"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar3Month"] = false;

        $scope.tempDataStatus = 1;//first preload completed
    }


    if ($scope.tempDataStatus == 1 && $scope.globalVars.ClickSensorsBtn.TempReq1Min == true){
        var count = 0;
        var k = "";
        var i = 0;
        for (k = $scope.globalVars.TempCount; k < chartData.length; k++) {
            for (i=0; i < chart.series.length - 1; i++) {
                $scope.tempI++;
                if (chartData[k].Name == chart.series[i].name) {
                    var x = new Date(chartData[k].Date[0], chartData[k].Date[1]-1, chartData[k].Date[2], chartData[k].Time[0], chartData[k].Time[1], chartData[k].Time[2], 0).getTime();
                    var index1 = i;
                    var index2 = k;
                    i;
                    k;
                    var pointsLength = chart.series[i].points.length;
                    var xDataLength = chart.series[i].xData.length;
                    if (xDataLength == 0) {
                        chart.series[i].addPoint([x, chartData[k].Value], true, false);
                    }
                    else {
                        if (chart.series[i].xData[xDataLength - 1] < x) {
                            count++;
                            chart.series[i].addPoint([x, chartData[k].Value], true, false);
                            if (i != 0 || chartData.length < 80) {
                                chart.series[0].addPoint([,], true, false);
                            }
                        }
                        if (count > 20) {
                            $scope.globalVars.TempCount = k;
                            $scope.chart.HudmLoadBar = true;
                            return;
                        }
                        else {
                            $scope.chart.HudmLoadBarVal++;
                            var loadingBar = ($scope.chart.HudmLoadBarVal * 100) / chartData.length;
                            if (loadingBar > 100) {
                                loadingBar = 100;
                            }
                            if (loadingBar < 0) {
                                loadingBar = 0;
                            }
                            // $scope.$$prevSibling.bar1.set(loadingBar);
                        }
                    }
                }
            }
        }
    }
}