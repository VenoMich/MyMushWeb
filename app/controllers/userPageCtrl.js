/**
 * Created by VenoM on 28.03.2017.
 */
function Ctrl($scope) {
    $scope.message = "Waiting 2000ms for update";

    setTimeout(function () {
        $scope.$apply(function () {
            $scope.message = "Timeout called!";
        });
    }, 2000);
}


app.controller('UserpageController', function ($scope, $http, globalVars, NgTableParams, $filter, $css) {

    //**********************vars START********************************
    var conf={// http req param
        timeout: 600, // 600 миллисекунд
        withCredentials: true,
        login:    $scope.inputtextLogin,
        password: $scope.inputtextPassword,
        Customer_ID: "",
    };
    $scope.globalVars = globalVars;
    $scope.relayUserName = "";//user inputtext for relay name
    $scope.buttonIndex = false;
    $scope.temp = "";
    $scope.showSenTable = ""; //
    $scope.relayClickEvent = ""; // relays table button flag
    $scope.sensorsClickEvent ="";// sensors table button flag
    $scope.ShowTempData = false;
    $scope.ShowHudmData = false;
    $scope.ShowSlaveData = false;
    $scope.updateFlag = true;
    $scope.message = "Loading Data from server...";
    $scope.nowDate = new Date();
    $scope.nowTime = $scope.nowDate.getTime();
    $scope.indexOfReq = 0;//count of server response
    $scope.OptionWarningDeltaT = 1;//user hysteresis value T
    $scope.OptionWarningDeltaH = 3;//user hysteresis value H
    $scope.HudmDataStatus = 0;

    this.cols = [
        { field: "name", title: "Name", show: true },
        { field: "age", title: "Age", show: true },
        { field: "money", title: "Money", show: true }
    ];
    //**********************vars END********************************

    //***********************component logTable bindings START*************************
    var photo = {
        imgpath: "ALARM.png",
    };

    $scope.photo = photo;
    this.photo = photo;
    $scope.test = "testttttt";
    this.test =  $scope.test;
    this.scope = $scope;






    //**********************component logTable bindings END********************************

    $scope.processScaleBar = function($event, elem){
        processScaleBar($event, $scope, elem);
        graphDyn($scope, $http);
    }

    //************************* update sensors, alarms, warnings, events on cur page
    $scope.updateDataEvent = function()
    {
        $scope.globalVars.ClickSensorsBtn["HudmReq1Min"] = false;
        $scope.HudmDataStatus = 0;
        if ($scope.globalVars.processBar.hudm.lastPos == ""){
            $scope.globalVars.ClickSensorsBtn["HudmReq1Min"] = false;
            $scope.globalVars.ClickSensorsBtn["HudmBar1Hour"] = true;
            $scope.globalVars.processBar.hudm.HudmBar1Hour = true;
            $scope.HudmDataStatus = 0;
        }
        switch ($scope.globalVars.processBar.hudm.lastPos)//remain last state of processBar
        {
            case "HudmBar1Hour"://Name of Sensor
                $scope.globalVars.processBar.hudm.HudmBar1Hour = true;
                $scope.globalVars.ClickSensorsBtn["HudmBar1Hour"] = true;
                break
            case "HudmBar6Hour"://Lower Value Alarm event
                $scope.globalVars.processBar.hudm.HudmBar6Hour = true;
                $scope.globalVars.ClickSensorsBtn["HudmBar6Hour"] = true;
                break
            case "HudmBar1Day"://Uppwer Value Alarm event
                $scope.globalVars.processBar.hudm.HudmBar1Day = true;
                $scope.globalVars.ClickSensorsBtn["HudmBar1Day"] = true;
                break
            case "HudmBar1Week"://Delta Value WARNING event
                $scope.globalVars.processBar.hudm.HudmBar1Week = true;
                $scope.globalVars.ClickSensorsBtn["HudmBar1Week"] = true;
                break
            case "HudmBar3Month"://spare
                $scope.globalVars.processBar.hudm.HudmBar3Month = true;
                $scope.globalVars.ClickSensorsBtn["HudmBar3Month"] = true;
                break
            default: break
        }
        $scope.globalVars.ClickSensorsBtn["TempReq1Min"] = false;
        $scope.tempDataStatus = 0;
        if ($scope.globalVars.processBar.temp.lastPos == ""){
            $scope.globalVars.ClickSensorsBtn["TempReq1Min"] = false;
            $scope.globalVars.ClickSensorsBtn["TempBar1Hour"] = true;
            $scope.globalVars.processBar.temp.TempBar1Hour = true;
            $scope.tempDataStatus = 0;
        }
        switch ($scope.globalVars.processBar.temp.lastPos)//remain last state of processBar
        {
            case "TempBar1Hour"://Name of Sensor
                $scope.globalVars.processBar.temp.TempBar1Hour = true;
                $scope.globalVars.ClickSensorsBtn["TempBar1Hour"] = true;
                break
            case "TempBar6Hour"://Lower Value Alarm event
                $scope.globalVars.processBar.temp.TempBar6Hour = true;
                $scope.globalVars.ClickSensorsBtn["TempBar6Hour"] = true;
                break
            case "TempBar1Day"://Uppwer Value Alarm event
                $scope.globalVars.processBar.temp.TempBar1Day = true;
                $scope.globalVars.ClickSensorsBtn["TempBar1Day"] = true;
                break
            case "TempBar1Week"://Delta Value WARNING event
                $scope.globalVars.processBar.temp.TempBar1Week = true;
                $scope.globalVars.ClickSensorsBtn["TempBar1Week"] = true;
                break
            case "TempBar3Month"://spare
                $scope.globalVars.processBar.temp.TempBar3Month = true;
                $scope.globalVars.ClickSensorsBtn["TempBar3Month"] = true;
                break
            default: break
        }

        // this.tableParams = $scope.tableParams;
        this.data = $scope.globalVars.ClickEvents;
        graphDyn($scope, $http);
        $scope.updateFlag = false;
        setInterval(function () {
            $scope.$apply(function ($scope) {
                // $scope.globalVars.data[2].uid = $scope.globalVars.data[2].uid + "1";
                if ($scope.globalVars.logLoadFlag == false && $scope.globalVars.broodLog.length != 0){
                    // $scope.tableParams = new NgTableParams({}, { dataset: $scope.globalVars.broodLog});
                    this.data = $scope.globalVars.broodLog;
                    $scope.tableParams = new NgTableParams({
                            paginationMaxBlocks: 5,
                            paginationMinBlocks: 2,
                            page: 1,
                            count: 5,
                    },  {dataset: $scope.globalVars.broodLog,
                         counts: [5, 10, 20],
                        }
                    );
                    this.data = $scope.globalVars.broodLog;
                    this.tableParams = $scope.tableParams;
                    $scope.globalVars.logLoadFlag = true;
                }
                updateData($http, $scope, conf);
            });
        }, 1000);
    };

    //*********************** **********************************
    getSensors ($http, $scope, conf);



    //************************* relays table userName button event
    $scope.$on('relaysClickEvent', function(){
        $scope.log = 'relaysClickEvent';
        var counter = 0;
        for (var key in $scope.globalVars.ClickEvents) {//change all menu's buttons flags to false
            if(key == "sensorsClickEvent"){
                $scope.globalVars.ClickEvents[key] = false;
            }
        }
        $scope.globalVars.ClickEvents.relaysClickEvent = true;//change clicked button to true
    });
    $scope.relaysClick = function()
    {
        // $scope.$broadcast('userpageevent', data);
        $scope.$emit('relaysClickEvent');
    };
    //*********************** **********************************

    //*********************** sensors table userName button event
    $scope.$on('sensorsClickEvent', function(){

        $scope.log = 'sensorsClickEvent';
        for (var key in $scope.globalVars.ClickEvents) {//change all menu's buttons flags to false
            if(key == "relaysClickEvent"){
                $scope.globalVars.ClickEvents[key] = false;
            }
        }
        $scope.globalVars.ClickEvents.sensorsClickEvent = true;//change clicked button to true

    });
    $scope.sensorsClick = function()
    {
        // $scope.$broadcast('userpageevent', data);
        $scope.$emit('sensorsClickEvent');
    };
    //*********************** **********************************
    //func for ng-if sensors table
    $scope.sensorsClickTemp = function()
    {
        if($scope.globalVars.ClickEvents.sensorsClickEvent == true)
        {
            return true;
        }
        else {return false;}
    };
    //func for ng-if relays table
    $scope.relaysClickTemp = function()
    {
        if($scope.globalVars.ClickEvents.relaysClickEvent == true)
        {
            return true;
        }
        else {return false;}
    };
    $scope.count=0;
    // $scope.mychart = MyCtrl();
//***********************SENSOR-TABLE*******************************************************************************
    $scope.sensorUserName = ""; // name of sensor
    // $scope.globalVars.sensorsFinal = $scope.sensors;
    $scope.ProcessSensorsBtn = function ($index, elem) {//process doubleclick
        var sensor = this.sensor;
        switch (elem)
        {
            case "ShowTempData"://Name of Sensor
                processShowTempData($index, sensor);
                break
            case "ShowHudmData"://Name of Sensor
                processShowHudmData($index, sensor);
                break
            case "ShowSlaveData"://Name of Sensor
                processShowSlaveData($index, sensor);
                break
            case "ShowErrData"://Show errors in alarm component by click on ERR icon
                processShowErrData($index, sensor);
                break
            case "ShowWarningData"://Show warnings in alarm component by click on WARNING icon
                processShowWarningData($index, sensor);
                break
            case "ShowSenMenu"://Name of Sensor
                processShowSenMenu($index, sensor);
                break
            case "ShowAlarmMenu"://Name of Sensor
                processShowAlarmMenu($index, sensor);
                break
            case "SensorName"://Name of Sensor
                processName($index, sensor);
                break
            case "LowerValue"://Lower Value Alarm event
                processLowerValue($index,  sensor);
                break
            case "UpperValue"://Upper Value Alarm event
                processUpperValue($index,  sensor);
                break
            case "Delta"://Upper Value Alarm event
                processDelta($index,  sensor);
                break
            case "ERR":// Alarm  event
                processErr($index,  sensor);
                break
            case "WARNING":// Warning  event
                processWarning($index,  sensor);
                break
            case "Spare"://spare
                processR($index,  sensor);
                break
            default: break
        }
        function processShowErrData ($index, sensor){// save Alarm Menu data, add "ClickShowTempData" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if ($scope.sensors[i].Status == "ERR")
                {
                    var tempArr = ({
                            Name: $scope.sensors[i].Name,
                            Status: $scope.sensors[i].Status,
                            Date: $scope.sensors[i].Date,
                            Time: $scope.sensors[i].Time,
                            Description: 'Превышено значение температуры! Превышено значение температурыПревышено значение температуры!',
                            Acknowledge: '',
                            Active: true,
                            ErrBlink: true,
                            ErrCode: '001',
                            AlarmBackgroundColor: {'opacity':'1', 'backgroundColor':'#FFA07A', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'},
                        });
                    var flag = false;
                    if ($scope.globalVars.alarms.err.length != 0){
                        for (j = 0; j < $scope.globalVars.alarms.err.length; j++){
                            if(tempArr.Name == $scope.globalVars.alarms.err[j].Name && tempArr.Date == $scope.globalVars.alarms.err[j].Date && tempArr.Time == $scope.globalVars.alarms.err[j].Time){
                                flag = true;
                            }
                        }
                        if(flag == false){
                            $scope.globalVars.alarms.err.push(tempArr);
                        }
                    }
                    else{
                        $scope.globalVars.alarms.err.push(tempArr);
                    }

                }
            }
            $scope.globalVars.ClickEvents["ClickShowErr"] = true;
            $scope.globalVars.ClickEvents["ClickShowWarning"] = false;
            $scope.globalVars.ClickEvents.ClickShowAllEvents = false
            globalVars.ClickEvents.alarmClickMenu = true;
        }

        function processShowWarningData ($index, sensor){// save Alarm Menu data, add "ClickShowTempData" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if ($scope.sensors[i].Status == "WARNING")
                {
                    var tempArr = ({
                        Name: $scope.sensors[i].Name,
                        Status: $scope.sensors[i].Status,
                        Date: $scope.sensors[i].Date,
                        Time: $scope.sensors[i].Time,
                        Description: 'Почти достигнуто максимальное значение температуры!',
                        Acknowledge: '',
                        Active: true,
                        ErrBlink: '',
                        WarnBlink: true,
                        AlarmBackgroundColor: {'opacity':'1', 'backgroundColor':'#FFFF00', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'},
                    });
                    var flag = false;
                    if ($scope.globalVars.alarms.warning.length != 0){
                        for (j = 0; j < $scope.globalVars.alarms.warning.length; j++){
                           if(tempArr.Name == $scope.globalVars.alarms.warning[j].Name && tempArr.Date == $scope.globalVars.alarms.warning[j].Date && tempArr.Time == $scope.globalVars.alarms.warning[j].Time){
                               flag = true;
                           }
                        }
                        if(flag == false){
                            $scope.globalVars.alarms.warning.push(tempArr);
                        }
                    }
                    else{
                        $scope.globalVars.alarms.warning.push(tempArr);
                    }
                }
            }
            $scope.globalVars.ClickEvents["ClickShowErr"] = false;
            $scope.globalVars.ClickEvents["ClickShowWarning"] = true;
            $scope.globalVars.ClickEvents.ClickShowAllEvents = false
            globalVars.ClickEvents.alarmClickMenu = true;
        }

        function processShowTempData ($index, sensor){// save temterature Sensor Menu Data, add "ClickShowTempData" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if ($scope.sensors[i].Type == "ds18b20")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowTempData"] = true;
                }
                if ($scope.sensors[i].Type == "H")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowHudmData"] = false;
                }
                if ($scope.sensors[i].Type == "Slave")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowSlaveData"] = false;
                }
                $scope.sensors[i] = $scope.globalVars.sensorsFinal[i];
            }
            $scope.ShowTempData = true;
            $scope.ShowHudmData = false;
            $scope.ShowSlaveData = false;
            getSensors($http, $scope, conf);
        }

        function processShowHudmData ($index, sensor){// save hudm Sensor Menu Data, add "ClickShowHudmData" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if ($scope.sensors[i].Type == "H")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowHudmData"] = true;
                }
                if ($scope.sensors[i].Type == "ds18b20")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowTempData"] = false;
                }
                if ($scope.sensors[i].Type == "Slave")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowSlaveData"] = false;
                }
                $scope.sensors[i] = $scope.globalVars.sensorsFinal[i];
            }
            $scope.ShowTempData = false;
            $scope.ShowHudmData = true;
            $scope.ShowSlaveData = false;
            getSensors($http, $scope, conf);
        }

        function processShowSlaveData ($index, sensor){// save Slave sensor Menu Data, add "ClickShowSlaveData" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if ($scope.sensors[i].Type == "Slave")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowSlaveData"] = true;
                }
                if ($scope.sensors[i].Type == "ds18b20")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowTempData"] = false;
                }
                if ($scope.sensors[i].Type == "H")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowHudmData"] = false;
                }
            }
            $scope.ShowTempData = false;
            $scope.ShowHudmData = false;
            $scope.ShowSlaveData = true;
        }

        function processShowSenMenu ($index, sensor){// close sen menu; all flags goes to false
            for(i=0;i<$scope.sensors.length;i++)
            {
                if ($scope.sensors[i].Type == "Slave")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowSlaveData"] = false;
                }
                if ($scope.sensors[i].Type == "ds18b20")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowTempData"] = false;
                }
                if ($scope.sensors[i].Type == "H")
                {
                    $scope.globalVars.sensorsFinal[i]["ClickShowHudmData"] = false;
                }
            }
            $scope.ShowTempData = false;
            $scope.ShowHudmData = false;
            $scope.ShowSlaveData = false;
            getSensors($http, $scope, conf);
        }

        function processName ($index, sensor){// save Name of Sensor to DB, add option "ClickSensorsBtnName" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                {
                    $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnName"] = true;
                }
            }
        }

        function processLowerValue ($index, sensor){// save Lower Value of Sensor to DB, add option "ClickSensorsBtnLowerValue" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                {
                    $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnLowerValue"] = true;
                }
            }
        }

        function processUpperValue ($index, sensor){// save Lower Value of Sensor to DB, add option "ClickSensorsBtnUpperValue" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                {
                    $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnUpperValue"] = true;
                }
            }
        }

        function processDelta ($index, sensor){// save Delta value of Sensor to DB, add option "ClickSensorsBtnDelta" to sensorsFinal object
            for(i=0;i<$scope.sensors.length;i++)
            {
                if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                {
                    $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnDelta"] = true;
                }
            }
        }

        function processErr($index, sensor){
            if(sensor.Type == "ds18b20" && sensor.Value > sensor.Upper_Value || sensor.Type == "ds18b20" && sensor.Value < sensor.Lower_Value){
                var grad = String.fromCharCode(176);
                sensor.Value = +sensor.Value;
                sensor.Upper_Value = +sensor.Upper_Value;
                sensor.Lower_Value = +sensor.Lower_Value;
                if (sensor.Value > sensor.Upper_Value)
                {
                    alert ("Actual Temperature Value  ("+ sensor.Value +" "+ grad +"C) is more than Upper Value! ("+ sensor.Upper_Value +" "+ grad +"C) ");
                    sensor["ErrBlink"] = false;
                }
                if (sensor.Value < sensor.Lower_Value)
                {
                    alert ("Actual Temprerature Value ("+ sensor.Value +" "+ grad +"C) is less than Lower Value! ("+ sensor.Lower_Value +" "+ grad +"C) ");
                    sensor["ErrBlink"] = false;
                }
                // return;
            }
            if(sensor.Type =="H" && sensor.Value > sensor.Upper_Value || sensor.Value < sensor.Lower_Value){
                if (sensor.Value > sensor.Upper_Value)
                {
                    alert ("Actual Hudmunity Value ("+ sensor.Value +"%) is more than Upper Value! ("+ sensor.Upper_Value +"%) ");
                    sensor["ErrBlink"] = false;
                }
                if (sensor.Value < sensor.Lower_Value)
                {
                    alert ("Actual Hudmunity Value ("+ sensor.Value +"%) is less than Lower Value! ("+ sensor.Lower_Value +"%) ");
                    sensor["ErrBlink"] = false;
                }
                // return;
            }
            for (i = 0; i<globalVars.sensorsFinal.length; i++){
                if(globalVars.sensorsFinal[i].Name == sensor.Name && globalVars.sensorsFinal[i].Time == sensor.Time){
                    globalVars.sensorsFinal[i].ErrBlink = false;
                    globalVars.sensorsFinal[i].Acknowledge = true;
                    globalVars.sensorsFinal[i].AlarmBackgroundColor = {'opacity':'1', 'backgroundColor':'', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};

                }
            }
            for (i = 0; i<globalVars.alarms.err.length; i++){
                if(globalVars.alarms.err[i].Name == sensor.Name && globalVars.alarms.err[i].Time == sensor.Time){
                    globalVars.alarms.err[i].Acknowledge = true;
                    globalVars.alarms.err[i].ErrBlink = false;
                    globalVars.alarms.err[i].AlarmBackgroundColor = {'opacity':'1', 'backgroundColor':'', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};
                }
            }
            sensor["ErrBlink"] = false;
            return;
        }

        function processWarning($index, sensor){
            var deltaValL = Math.abs(sensor.Value) - sensor.Lower_Value ;
            var deltaValH = Math.abs(sensor.Upper_Value) - sensor.Value;
            if (deltaValH >= deltaValL){
                var delta = deltaValH;
            }
            else{
                var delta = deltaValL;
            }

            if(sensor.Type =="ds18b20"){
                grad = String.fromCharCode(176);
                alert ("Actual Temperature Value ("+ sensor.Value +" "+ grad +"C) is different from Upper/Lower border more than selected in options! ( "+ $scope.OptionWarningDeltaT +" "+ grad +"C ) ");
                sensor["WarnBlink"] = false;
                // return;
            }
            if(sensor.Type =="H"){
                alert ("Actual Hudmunity Value ("+ sensor.Value +"%) is different from Upper/Lower border more than selected in options! ("+ $scope.OptionWarningDeltaH +"%) ");
                sensor["WarnBlink"] = false;
                // return;
            }
            for (i = 0; i<globalVars.sensorsFinal.length; i++){
                if(globalVars.sensorsFinal[i].Name == sensor.Name && globalVars.sensorsFinal[i].Time == sensor.Time){
                    globalVars.sensorsFinal[i].WarnBlink = false;
                    globalVars.sensorsFinal[i].Acknowledge = true;
                    globalVars.sensorsFinal[i].AlarmBackgroundColor = {'opacity':'1', 'backgroundColor':'', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};

                }
            }
            for (i = 0; i<globalVars.alarms.warning.length; i++){
                if(globalVars.alarms.warning[i].Name == sensor.Name && globalVars.alarms.warning[i].Time == sensor.Time){
                    globalVars.alarms.warning[i].Acknowledge = true;
                    globalVars.alarms.warning[i].WarnBlink = false;
                    globalVars.alarms.warning[i].AlarmBackgroundColor = {'opacity':'1', 'backgroundColor':'', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};
                }
            }
            sensor["WarnBlink"] = false;
            return;
        }

    }

    $scope.ProcessInputName = function ($index, sensorUserName, keyCode, elem) {//process input
        if (elem == "logData"){
            if ($index != "" && sensorUserName != ""){
                if ($index.getTime() >= sensorUserName.getTime()){             //check for correct datetime input (startTime < endTime)
                    if ($scope.globalVars.language == "Russian"){
                        alert ("Начальная дата запроса должна быть раньше конечной!")
                    }
                    if ($scope.globalVars.language == "English"){
                        alert ("Set From and To of request correctly!")
                    }
                    return;
                }
                var dateStart = new String();
                dateStart = ($index.getFullYear() + "-" + ($index.getMonth() + 1) + "-" + $index.getDate()).toString();
                var dateEnd = new String();
                dateEnd = (sensorUserName.getFullYear() + "-" + (sensorUserName.getMonth() + 1) + "-" + sensorUserName.getDate()).toString();
                if (sensorUserName.getHours() == 0 && sensorUserName.getMinutes() == 0){
                    var timeStart = "00:00:00"
                }
                else{
                    var timeStart = new String();
                    timeStart = ($index.getHours() + ":" + $index.getMinutes() + ":" + "00").toString();
                }
                if (sensorUserName.getHours() == 0 && sensorUserName.getMinutes() == 0){
                    var timeEnd = "23:59:59"
                }
                else{
                    var timeEnd = new String();
                    timeEnd = (sensorUserName.getHours() + ":" + sensorUserName.getMinutes() + ":" + "00").toString();
                }
                var conf = {
                    dateStart: dateStart,
                    dateEnd: dateEnd,
                    timeStart: timeStart,
                    timeEnd: timeEnd,
                    customerId: userInfo.Customer_ID,
                    options: keyCode,
                };
                getSensorsLog ($scope, $http, conf, httpGet.getSensorsLog);


                return;
            }
            else{
                if ($scope.globalVars.language == "Russian"){
                    alert ("Укажите начало и конец запроса правильно! Время 00:00 - 00:00 - запрос за 24 часа.")
                }
                if ($scope.globalVars.language == "English"){
                    alert ("Set start and end datemime of request correctly!")
                }
                return;
            }
        }
        if(keyCode == 13 || keyCode.type == "click"){//if "enter" pressed
            if (keyCode.type == "click"){
                this.$parent.sensor = this.$parent.$parent.$parent.sensor;
            }
            $scope.sensorUserName = sensorUserName;
            if (keyCode.type == "click"){
                $scope.sensorUserName = this.$$prevSibling.sensorUserName;
                sensorUserName = this.$$prevSibling.sensorUserName;
            }
            if (keyCode.type == "click" || elem == "SensorName"){
                this.$parent.sensor = this.$parent.$parent.sensor;
            }
            switch (elem)
            {
                case "SensorName"://Name of Sensor
                    processName($index, this.$parent.sensor);
                    break
                case "LowerValue"://Lower Value Alarm event
                    processLowerValue($index,  this.$parent.sensor);
                    break
                case "UpperValue"://Uppwer Value Alarm event
                    processUpperValue($index,  this.$parent.sensor);
                    break
                case "Delta"://Delta Value WARNING event
                    processDelta($index,  this.$parent.sensor);
                    break
                case "Spare"://spare
                    processR($index,  this.sensor);
                    break
                default: break
            }

            function processName($index, sensor) {
                for(i=0;i<$scope.sensors.length;i++)
                {
                    if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                    {
                        var tempData = {};
                        for (var key in $scope.globalVars.sensorsFinal[i]) {//change all menu's buttons flags to false
                            if (key != "$$hashKey")
                            {
                                tempData[key] = $scope.globalVars.sensorsFinal[i][key];
                            }
                        }
                        $scope.positionIndex = i;
                        if(sensorUserName == undefined){
                            if (language == "Russian"){
                                alert("Вы пытаетесь ввести пустое поле! Введите корректное значение!");
                            }
                            if (language == "English"){
                                alert("You are try to enter empty field! Please, enter correct value!");
                            }
                            return;
                        }
                        tempData.Name = sensorUserName;
                        $scope.globalVars.sensorsFinal[i].ClickSensorsBtnName = false;
                        sensorUserName = "";
                        $scope.sensorUserName = "";
                        setSensorsName ($http, $scope, tempData);
                    }
                }
            }

            function processLowerValue($index, sensor) {
                // sensorUserName = parseInt(sensorUserName);
                sensorUserName = +sensorUserName;
                if (isNaN(sensorUserName) == true)//check for correct input
                {
                    if (language == "Russian"){
                        alert("Значение мин. тревоги должно быть числом!");
                    }
                    if (language == "English"){
                        alert("Lower Value must be a digit!");
                    }
                    $scope.sensorUserName = "";
                    sensorUserName = "";
                    return;
                }
                if(typeof sensorUserName != 'number')//is number
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Введите корректное значение мин. тревоги!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("Please, enter correct Lower Value!");
                    }
                    return;
                }
                if (sensorUserName < 50 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести значение мин. тевоги для влажности менее 50%. Вы уверены?");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Lower Level below 50%. Are you shure?");
                    }
                }
                if (sensorUserName > 98 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести значение мин. тевоги для влажности более 98%. Введите корректное значение!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Lower Level upper 98%. Please, enter correct value!");
                    }
                    return;
                }
                if (sensorUserName < 1 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести значение мин. тевоги для влажности менее 1%. Введите корректное значение!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Lower Level below 1%. Please, enter correct value!");
                    }
                    return;
                }
                grad = String.fromCharCode(176);
                if (sensorUserName < 20 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести значение мин. тевоги для температуры менее 20 "+grad+"C. Вы уверены?");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Lower Level below 20 "+grad+"C. Are you shure?");
                    }
                }
                if (sensorUserName > 30 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести значение мин. тевоги для температуры выше 20 "+grad+"C. Вы уверены?");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Lower Level upper 30 "+grad+"C. Are you shure?");
                    }
                }
                if (sensorUserName > 100 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести значение мин. тевоги для температуры выше 100 "+grad+"C. Введите корректное значение!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Lower Level upper 100 "+grad+"C. Please, enter correct value!");
                    }
                    return;
                }
                for(i=0;i<$scope.sensors.length;i++)
                {
                    if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                    {
                        if (sensorUserName >= $scope.globalVars.sensorsFinal[i]["Upper_Value"])
                        {
                            if ($scope.globalVars.language == "Russian"){
                                alert("Минимальное значение тревоги не может быть больше максимального! Введите корректное значение!");
                            }
                            if ($scope.globalVars.language == "English"){
                                alert("Lower Value can't be more than Upper Value! Please, enter correct value!");
                            }
                            return;
                        }
                        var tempData = {};
                        for (var key in $scope.globalVars.sensorsFinal[i]) {//change all menu's buttons flags to false
                            if (key != "$$hashKey")
                            {
                                tempData[key] = $scope.globalVars.sensorsFinal[i][key];
                            }
                        }
                        tempData.Lower_Value = sensorUserName;
                        $scope.globalVars.sensorsFinal[i].ClickSensorsBtnLowerValue = false;
                        $scope.sensorUserName = "";
                        $scope.positionIndex = i;
                        sensorUserName = "";
                        setLowerValue ($http, $scope, tempData);
                    }
                }
            }

            function processUpperValue($index, sensor) {
                // sensorUserName = parseInt(sensorUserName);
                sensorUserName = +sensorUserName;
                if (isNaN(sensorUserName) == true)//check for correct input
                {
                    alert("Upper Value must be a digit!");
                    $scope.sensorUserName = "";
                    sensorUserName = "";
                    return;
                }
                if(typeof sensorUserName != 'number')//is number
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Значение макс. тревоги должно быть числом!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("Please, enter correct Upper Value");
                    }
                    return;
                }
                if (sensorUserName < 50 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести макс. значение тревоги по влажности менее 50%. Вы уверены?");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Upper Level below 50%. Are you shure?");
                    }
                }
                if (sensorUserName > 99 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести макс. значение тревоги по влажности более 99%. Введите корректное значение!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Upper Level upper 99%. Please, enter correct value!");
                    }
                    return;
                }
                if (sensorUserName < 2 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести макс. значение тревоги по влажности менее 2%. Введите корректное значение!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Upper Level below 2%. Please, enter correct value!");
                    }
                    return;
                }
                grad = String.fromCharCode(176);
                if (sensorUserName < 20 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести макс. значение тревоги по температуре менее 20 "+grad+"C. Вы уверены?");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Upper Level below 20 "+grad+"C. Are you shure?");
                    }
                }
                if (sensorUserName > 30 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести макс. значение тревоги по температуре более 30 "+grad+"C. Вы уверены?");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Upper Level upper 30 "+grad+"C. Are you shure?");
                    }
                }
                if (sensorUserName > 100 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести макс. значение тревоги по температуре более 100 "+grad+"C. Введите корректное значение!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Upper Level upper 100 "+grad+"C. Please, enter correct value!");
                    }
                    return;
                }
                for(i=0;i<$scope.sensors.length;i++)
                {
                    if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                    {
                        if ($scope.globalVars.sensorsFinal[i]["Lower_Value"] >= sensorUserName)
                        {
                            if ($scope.globalVars.language == "Russian"){
                                alert("Значение макс. тревоги по влажности не может быть меньше значения мин. тревоги!. Введите корректное значение!");
                            }
                            if ($scope.globalVars.language == "English"){
                                alert("Upper Value can't be less than Lower Value! Please, enter correct value!");
                            }
                            return;
                        }
                        var tempData = {};
                        for (var key in $scope.globalVars.sensorsFinal[i]) {//change all menu's buttons flags to false
                            if (key != "$$hashKey")
                            {
                                tempData[key] = $scope.globalVars.sensorsFinal[i][key];
                            }
                        }
                        $scope.positionIndex = i;
                        tempData.Upper_Value = sensorUserName;
                        $scope.globalVars.sensorsFinal[i].ClickSensorsBtnUpperValue = false;
                        sensorUserName = "";
                        $scope.sensorUserName = "";
                        setUpperValue ($http, $scope, tempData);
                    }
                }
            }

            function processDelta($index, sensor) {
                // sensorUserName = parseInt(sensorUserName);
                sensorUserName = +sensorUserName;
                if (isNaN(sensorUserName) == true)//check for correct input
                {
                    alert("Delta must be a digit!");
                    $scope.sensorUserName = "";
                    sensorUserName = "";
                    return;
                }
                if(typeof sensorUserName != 'number')//is number
                {
                    alert("Please, enter correct Delta");
                    return;
                }
                if (sensorUserName < 0 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести порог предупреждения по Влажности меньше 0%. Введите корректное значение (0 - 5)!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Delta below 0%. Please, enter correct value (0 - 5)!");
                    }
                    return;
                }
                if (sensorUserName > 5 && sensor.Type == 'H')//is correct for H
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести порог предупреждения по Влажности больше 5%. Введите корректное значение (0 - 5)!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Hudmunity Delta upper 99%. Please, enter correct value (0 - 5)!");
                    }
                    return;
                }
                grad = String.fromCharCode(176);
                if (sensorUserName < 0 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести порог предупреждения по Температуре меньше 0 "+grad+"C. Введите корректное значение (0 - 2.5)!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Delta below 0 "+grad+"C. Please, enter correct value (0 - 2.5)!");
                    }
                    return;
                }
                if (sensorUserName > 2.5 && sensor.Type == 'ds18b20')//is correct for T
                {
                    if ($scope.globalVars.language == "Russian"){
                        alert("Вы пытаетесь ввести порог предупреждения по Температуре выше 2.5 "+grad+"C. Введите корректное значение (0 - 2.5)!");
                    }
                    if ($scope.globalVars.language == "English"){
                        alert("You are try to enter Temperature Delta upper 2.5 "+grad+"C. Please, enter correct value (0 - 2.5)!");
                    }
                    return;
                }
                for(i=0;i<$scope.sensors.length;i++)
                {
                    if (sensor.Address == $scope.sensors[i].Address && sensor.Slave_ID == $scope.sensors[i].Slave_ID && sensor.Type == $scope.sensors[i].Type)
                    {
                        var tempData = {};
                        for (var key in $scope.globalVars.sensorsFinal[i]) {//copy without hashkey
                            if (key != "$$hashKey")
                            {
                                tempData[key] = $scope.globalVars.sensorsFinal[i][key];
                            }
                        }
                        $scope.positionIndex = i;
                        tempData.Delta = sensorUserName;
                        $scope.globalVars.sensorsFinal[i].ClickSensorsBtnDelta = false;
                        sensorUserName = "";
                        $scope.sensorUserName = "";
                        setDelta ($http, $scope, tempData);
                    }
                }
            }
        }
    }


    
    //***********************RELAYS-TABLE*******************************************************************************
    //check relay state before print cell color in relays-table
    $scope.checkRelayState = function($index)
    {
        if($scope.relaysAvailable[$index].Value == "ON")
        {
            return true;
        }
        else {return false;}
    }
    //inject object's properties (index, relayUserName)
    $scope.RelayUsernameSetClick = function($index, relayUserName)
    {
        for(i=0;i<$scope.relaysAvailable.length;i++)
        {
            if (this.relay.Address == $scope.relaysAvailable[i].Address && this.relay.Slave_ID == $scope.relaysAvailable[i].Slave_ID)
            {
                $scope.temp = $scope.relaysAvailable[i];
                $scope.temp["RelayButton"] = $index;
                $scope.temp["Username"] = relayUserName;
                relayUserName = "";
            }
        }
    };
    //change object's properties: relay Status (ON to OFF/ OFF to ON)
    $scope.RelayUsernameChangeStatus = function($index)
    {
        for(i=0;i<$scope.relaysAvailable.length;i++)
        {
            if (this.relay.Address == $scope.relaysAvailable[i].Address && this.relay.Slave_ID == $scope.relaysAvailable[i].Slave_ID)
            {
                $scope.temp = $scope.relaysAvailable[i];
                if ($scope.temp.RelayButton == undefined)
                {
                    $scope.temp["RelayButton"] = $index;
                }
                if ($scope.temp.Value == "ON")
                {
                    $scope.temp.Value = "OFF";
                }
                else {$scope.temp.Value = "ON";}
            }
        }
    };
});

/*********************************************************************
 * Function:        updateData($http, $scope, conf)
 * PreCondition:     myMush server started
 * Input:            $http, $scope, conf
 * Return^           data or error
 * Overview:         This function receive sensors data
 * Note:
 ********************************************************************/
function updateData($http, $scope, conf)
{
    var updateDate= new Date();
    var updateTime = updateDate.getTime();
    var delta = updateTime - $scope.nowTime;
    delta = Math.floor(delta/1000);
    $scope.delta = delta;
    if ($scope.globalVars.sensorsFinal != undefined)
    {
        for(i=0;i<$scope.globalVars.sensorsFinal.length;i++)
        {
            if ($scope.globalVars.sensorsFinal[i]["ClickSensorsBtnName"] == true || $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnLowerValue"] == true || $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnUpperValue"] == true || $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnDelta"] == true)// froze data update until user input fields
            {
                var updateDataFlag = false;
                if (delta >25)//timeout for data input 20 sec
                {
                    updateDataFlag = true;
                }
            }
        }
    }
    if (delta >3 && updateDataFlag !=false)
    {
        setTimeout(function () {
            $scope.$apply(function () {
                if ($scope.globalVars.sensorsFinal != undefined && $scope.timeoutFlag == false)
                {
                    for(i=0;i<$scope.globalVars.sensorsFinal.length;i++)
                    {
                        if ($scope.globalVars.sensorsFinal[i]["ClickSensorsBtnName"] == true || $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnLowerValue"] == true || $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnUpperValue"] == true || $scope.globalVars.sensorsFinal[i]["ClickSensorsBtnDelta"] == true)// froze data update until user input fields
                        {
                            $scope.timeoutflag = true;
                            return;
                        }
                    }
                }
                $scope.timeoutFlag == false;
                getSensors ($http, $scope, conf);
            });
        }, 200);
        $scope.nowTime = updateDate.getTime();
    }
}

/*********************************************************************
 * Function:        getSensors($http, $scope, conf)
 * PreCondition:     myMush server started
 * Input:            $http, $scope, conf
 * Return^           data or error
 * Overview:         This function receive sensors data
 * Note:
 ********************************************************************/
function getSensors($http, $scope, conf) {
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + httpGet.getSensors; // host + req string
    conf.Customer_ID = userInfo.Customer_ID;
     $http({
        method: 'PUT',
        url: address,
        data: conf,
    }).then(function(sensors) {
        //************if data received good - check pass and login in database***********
         $scope.globalVars.data = [ { uid: 'User 11', name: 'Name2 13', area: "Area 1"},
             { uid: 'User 12', name: 'Name 12', area: 'Area 1'},
             { uid: 'User 21', name: 'Name 21', area: 'Area 2'},
             { uid: 'User 22', name: 'Name 22', area: 'Area 2'}
         ];
         $scope.sensors = sensors.data;
         for(i=0;i<sensors.length;i++)
         {
             // var tempDate = $scope.sensors[i].Date.split('-');
             // var temp = tempDate[2].substring(0, 2);//удалить элемент строки (контрольную сумму,OD,0A)
             // tempDate[2] = temp;
             // tempDate = tempDate.join('-');
             // $scope.sensors[i].Date = tempDate;
             // $scope.sensors;
         }
         $scope.indexOfReq++;


         for (i=0;i<$scope.sensors.length;i++){
             if ($scope.globalVars.sensorsFinal[i] != undefined){
                 if ($scope.globalVars.sensorsFinal[i].Address == $scope.sensors[i].Address && $scope.globalVars.sensorsFinal[i].Slave_ID == $scope.sensors[i].Slave_ID && $scope.globalVars.sensorsFinal[i].Type == $scope.sensors[i].Type){
                     $scope.sensors[i]["WarnBlink"] = $scope.globalVars.sensorsFinal[i].WarnBlink;
                     $scope.sensors[i]["ErrBlink"] = $scope.globalVars.sensorsFinal[i].ErrBlink;
                     $scope.sensors[i]["AlarmBackgroundColor"] = $scope.globalVars.sensorsFinal[i].AlarmBackgroundColor;
                     if ($scope.globalVars.sensorsFinal[i].ClickShowTempData != undefined){    //ClickShowTempData
                         $scope.sensors[i]["ClickShowTempData"] = $scope.globalVars.sensorsFinal[i].ClickShowTempData;
                     }
                     if ($scope.globalVars.sensorsFinal[i].ClickShowHudmData != undefined){    // ClickShowHudmData
                         $scope.sensors[i]["ClickShowHudmData"] = $scope.globalVars.sensorsFinal[i].ClickShowHudmData;
                     }
                     if ($scope.globalVars.sensorsFinal[i].ClickShowSlaveData != undefined){    // ClickShowSlaveData
                         $scope.sensors[i]["ClickShowSlaveData"] = $scope.globalVars.sensorsFinal[i].ClickShowSlaveData;
                     }
                     if ($scope.globalVars.sensorsFinal[i].ShowAddInfo != undefined){          // ShowAddInfo
                         $scope.sensors[i]["ShowAddInfo"] = $scope.globalVars.sensorsFinal[i].ShowAddInfo;
                     }
                     if ($scope.globalVars.sensorsFinal[i].ArrowEnter == undefined){           // ArrowEnter
                     }
                     else{
                         $scope.sensors[i]["ArrowEnter"] = $scope.globalVars.sensorsFinal[i].ArrowEnter;
                     }
                     if ($scope.globalVars.sensorsFinal[i].showMousenterIcons == undefined){           // showMousenterIcons
                     }
                     else{
                         $scope.sensors[i]["showMousenterIcons"] = $scope.globalVars.sensorsFinal[i].showMousenterIcons;
                     }
                     if ($scope.globalVars.sensorsFinal[i].ClickSensorsBtnName == undefined){           // ClickSensorsBtnName
                     }
                     else{
                         $scope.sensors[i]["ClickSensorsBtnName"] = $scope.globalVars.sensorsFinal[i].ClickSensorsBtnName;
                     }
                 }
             }
         }
         $scope.globalVars.sensorsFinal = $scope.sensors;

         for(i=0;i<$scope.globalVars.sensorsFinal.length;i++)
         {
             if ($scope.globalVars.sensorsFinal[i]["Status"] == undefined)// froze data update until user input fields
             {
                 $scope.globalVars.sensorsFinal[i]["Status"] = "";
             }

             if ($scope.globalVars.sensorsFinal[i]["Type"] == "ds18b20")
             {
                 var Lower_Value = +$scope.globalVars.sensorsFinal[i]["Lower_Value"];
                 var Upper_Value = +$scope.globalVars.sensorsFinal[i]["Upper_Value"];
                 var deltaH = +$scope.globalVars.sensorsFinal[i]["Upper_Value"] + $scope.OptionWarningDeltaT;
                 var deltaL = +$scope.globalVars.sensorsFinal[i]["Lower_Value"] - $scope.OptionWarningDeltaT;
                 var value = +$scope.globalVars.sensorsFinal[i]["Value"];
                 if (value < deltaH && value > deltaL )
                 {
                     $scope.globalVars.sensorsFinal[i]["Status"] = "WARNING";
                     if ($scope.globalVars.sensorsFinal[i]["WarnBlink"] != false)
                     {
                         $scope.globalVars.sensorsFinal[i]["WarnBlink"] = true;
                         $scope.globalVars.sensorsFinal[i]["AlarmBackgroundColor"] = {'opacity':'1', 'backgroundColor':'#FFFF00', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};
                     }
                 }
                 else{
                     $scope.globalVars.sensorsFinal[i]["Status"] = "ERR";
                     if ($scope.globalVars.sensorsFinal[i]["ErrBlink"] != false)
                     {
                         $scope.globalVars.sensorsFinal[i]["ErrBlink"] = true;
                         $scope.globalVars.sensorsFinal[i]["AlarmBackgroundColor"] = {'opacity':'1', 'backgroundColor':'#FFA07A', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};
                     }
                 }
                 if (value >= Lower_Value && value <= Upper_Value)
                 {
                     $scope.globalVars.sensorsFinal[i]["Status"] = "OK";
                 }
             }
             if ($scope.globalVars.sensorsFinal[i]["Type"] == "H")
             {
                 $scope.globalVars.sensorsFinal[i]["Lower_Value"] = +$scope.globalVars.sensorsFinal[i]["Lower_Value"];
                 $scope.globalVars.sensorsFinal[i]["Upper_Value"] = +$scope.globalVars.sensorsFinal[i]["Upper_Value"];
                 $scope.globalVars.sensorsFinal[i]["Value"] = +$scope.globalVars.sensorsFinal[i]["Value"];
                 if ($scope.globalVars.sensorsFinal[i]["Value"] <= ($scope.globalVars.sensorsFinal[i]["Upper_Value"] + $scope.OptionWarningDeltaH)  && $scope.globalVars.sensorsFinal[i]["Value"] > ($scope.globalVars.sensorsFinal[i]["Lower_Value"] - $scope.OptionWarningDeltaH) )
                 {
                     $scope.globalVars.sensorsFinal[i]["Status"] = "WARNING";
                     if ($scope.globalVars.sensorsFinal[i]["WarnBlink"] != false)
                     {
                         $scope.globalVars.sensorsFinal[i]["WarnBlink"] = true;
                         $scope.globalVars.sensorsFinal[i]["AlarmBackgroundColor"] = {'opacity':'1', 'backgroundColor':'#FFFF00', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};
                     }
                 }
                 else{
                     $scope.globalVars.sensorsFinal[i]["Status"] = "ERR";
                     if ($scope.globalVars.sensorsFinal[i]["ErrBlink"] != false)
                     {
                         $scope.globalVars.sensorsFinal[i]["ErrBlink"] = true;
                         $scope.globalVars.sensorsFinal[i]["AlarmBackgroundColor"] = {'opacity':'1', 'backgroundColor':'#FFA07A', 'lineHeight':'0.9em', 'marginOutside':'0pt', 'padding-bottom':'-100pt'};
                     }
                 }
                 if ($scope.globalVars.sensorsFinal[i]["Value"] <= $scope.globalVars.sensorsFinal[i]["Upper_Value"] && $scope.globalVars.sensorsFinal[i]["Value"] >= $scope.globalVars.sensorsFinal[i]["Lower_Value"])
                 {
                     $scope.globalVars.sensorsFinal[i]["Status"] = "OK";
                 }
             }

         }

         $scope.globalVars.alarms.err.splice(0,$scope.globalVars.alarms.err.length);//delete old alarms
         $scope.globalVars.alarms.warning.splice(0,$scope.globalVars.alarms.warning.length);//delete old warnings
         for (i = 0; i < $scope.sensors.length; i++){                 //update warnings in alarm component
             if ($scope.sensors[i].Status == "WARNING")
             {
                 var tempArr = ({
                     Name: $scope.sensors[i].Name,
                     Status: $scope.sensors[i].Status,
                     Date: $scope.sensors[i].Date,
                     Time: $scope.sensors[i].Time,
                     Description: 'Почти достигнуто максимальное значение температуры!',
                     Acknowledge: '',
                     ErrBlink: $scope.sensors[i].ErrBlink,
                     WarnBlink: $scope.sensors[i].WarnBlink,
                     AlarmBackgroundColor: $scope.sensors[i].AlarmBackgroundColor,
                 });
                 var flag = false;
                 if ($scope.globalVars.alarms.warning.length != 0){
                     for (j = 0; j < $scope.globalVars.alarms.warning.length; j++){
                         if(tempArr.Name == $scope.globalVars.alarms.warning[j].Name && tempArr.Date == $scope.globalVars.alarms.warning[j].Date && tempArr.Time == $scope.globalVars.alarms.warning[j].Time){
                             flag = true;
                         }
                     }
                     if(flag == false){
                         $scope.globalVars.alarms.warning.push(tempArr);
                     }
                 }
                 else{
                     $scope.globalVars.alarms.warning.push(tempArr);
                 }
             }
             if ($scope.sensors[i].Status == "ERR")             //update alarms in alarm component
             {
                 var tempArr = ({
                     Name: $scope.sensors[i].Name,
                     Status: $scope.sensors[i].Status,
                     Date: $scope.sensors[i].Date,
                     Time: $scope.sensors[i].Time,
                     Description: 'Превышено значение температуры! Превышено значение температурыПревышено значение температуры!',
                     Acknowledge: '',
                     ErrBlink: $scope.sensors[i].ErrBlink,
                     AlarmBackgroundColor: $scope.sensors[i].AlarmBackgroundColor,
                 });
                 var flag = false;
                 if ($scope.globalVars.alarms.err.length != 0){
                     for (j = 0; j < $scope.globalVars.alarms.err.length; j++){
                         if(tempArr.Name == $scope.globalVars.alarms.err[j].Name && tempArr.Date == $scope.globalVars.alarms.err[j].Date && tempArr.Time == $scope.globalVars.alarms.err[j].Time){
                             flag = true;
                         }
                     }
                     if(flag == false){
                         $scope.globalVars.alarms.err.push(tempArr);
                     }
                 }
                 else{
                     $scope.globalVars.alarms.err.push(tempArr);
                 }
             }
         }

         //sort by DateTime
         if ($scope.globalVars.alarms.err.length != 0){
             sortByDateTime ($scope, "Alarm", $scope.globalVars.alarms.err);
         }
         if ($scope.globalVars.alarms.warning.length != 0){
             sortByDateTime ($scope, "Warning", $scope.globalVars.alarms.warning);
         }

         // graphTemp($scope);
         // graphHudm($scope);
         // drawChart($scope);
         $scope.serverConnected = true;
         $scope.message = " Server Connection Status: " + $scope.indexOfReq;// printf server connection status, count
         if ($scope.indexOfReq == 1000000)
         {
             $scope.indexOfReq = 0;
         }
    },function (err) {
        console.log('getSensors ERR');
         $scope.serverConnected = false;
        console.log(err);
    })
}

/*********************************************************************
 * Function:        setSensorsName($http, $scope, conf)
 * PreCondition:     myMush server started
 * Input:            $http, $scope, conf
 * Return^           data or error
 * Overview:         This function set name of sensors to DB
 * Note:
 ********************************************************************/
function setSensorsName($http, $scope, conf) {
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + httpGet.setSensorName; // host + req string
    $http({
        method: 'PUT',
        url: address,
        data: conf,
    }).then(function(sensors) {
        var sensors = sensors.data;
        if (sensors == "true")
        {
            $scope.globalVars.sensorsFinal[$scope.positionIndex]["Name"] = $scope.sensorUserName;
            if (language == "Russian"){
                alert("Новое имя датчика установлено!");
            }
            if (language == "English"){
                alert ("New Sensor Name was set!");
            }
        }
        else{
            if (language == "Russian"){
                alert("Ошибка: БД не обновлена! Новое значение предупреждения не установлено!");
            }
            if (language == "English"){
                alert ("Error: DB not updated! New value can't be set!");
            }
        }
    },function (err) {
        console.log('Server not response: setSensorsName ERR');
        console.log(err);
    });
}

/*********************************************************************
 * Function:        setLowerValue($http, $scope, conf)
 * PreCondition:     myMush server started
 * Input:            $http, $scope, conf
 * Return^           data or error
 * Overview:         This function set LowerValue for current sensor to DB
 * Note:
 ********************************************************************/
function setLowerValue ($http, $scope, conf){
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + httpGet.setLowerValue; // host + req string
    $http({
        method: 'PUT',
        url: address,
        data: conf,
    }).then(function(sensors) {
        var sensors = sensors.data;
        if (sensors == "true")
        {
        $scope.globalVars.sensorsFinal[$scope.positionIndex]["Lower_Value"] = $scope.sensorUserName;
            if (language == "Russian"){
                alert("Новое значение Мин. Тревоги установлено!");
            }
            if (language == "English"){
                alert ("New Lower Value was set!");
            }
        }
        else{
            if (language == "Russian"){
                alert("Ошибка: БД не обновлена! Новое значение предупреждения не установлено!");
            }
            if (language == "English"){
                alert ("Error: DB not updated! New value can't be set!");
            }
        }
    },function (err) {
        console.log('Server not response: setLowerValue ERR');
        console.log(err);
    });
}

/*********************************************************************
 * Function:        setUpperValue($http, $scope, conf)
 * PreCondition:     myMush server started
 * Input:            $http, $scope, conf
 * Return^           data or error
 * Overview:         This function set UpperValue for current sensor to DB
 * Note:
 ********************************************************************/
function setUpperValue ($http, $scope, conf){
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + httpGet.setUpperValue; // host + req string
    $http({
        method: 'PUT',
        url: address,
        data: conf,
    }).then(function(sensors) {
        var sensors = sensors.data;
        if (sensors == "true")
        {
            $scope.globalVars.sensorsFinal[$scope.positionIndex]["Upper_Value"] = $scope.sensorUserName;
            if (language == "Russian"){
                alert("Новое значение Макс. Тревоги установлено!");
            }
            if (language == "English"){
                alert ("New Upper Value was set!");
            }
        }
        else{
            if (language == "Russian"){
                alert("Ошибка: БД не обновлена! Новое значение предупреждения не установлено!");
            }
            if (language == "English"){
                alert ("Error: DB not updated! New value can't be set!");
            }
        }
        //************if data received good - check pass and login in database***********
    },function (err) {
        console.log('Server not response: setUpperValue ERR');
        console.log(err);
    });
}

/*********************************************************************
 * Function:        setDelta($http, $scope, conf)
 * PreCondition:     myMush server started
 * Input:            $http, $scope, conf
 * Return^           data or error
 * Overview:         This function set Delta value for current sensor to DB
 * Note:
 ********************************************************************/
function setDelta ($http, $scope, conf){
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + httpGet.setDelta; // host + req string
    $http({
        method: 'PUT',
        url: address,
        data: conf,
    }).then(function(sensors) {
        var sensors = sensors.data;
        if (sensors == "true")
        {
            $scope.globalVars.sensorsFinal[$scope.positionIndex]["Delta"] = $scope.sensorUserName;
            if (language == "Russian"){
                alert("Новое значения предупреждения установлено!");
            }
            if (language == "English"){
                alert ("New Delta Value was set!");
            }
        }
        else{
            if (language == "Russian"){
                alert("Ошибка: БД не обновлена! Новое значение предупреждения не установлено!");
            }
            if (language == "English"){
                alert ("Error: DB not updated! New Delta value can't be set!");
            }
        }
        //************if data received good - check pass and login in database***********
    },function (err) {
        console.log('Server not response: setUpperValue ERR');
        console.log(err);
    });
}

/*********************************************************************
 * Function:        hudmGraphBar($index, event)
 * PreCondition:     myMush server started
 * Input:            $index, $event of DOM
 * Return^
 * Overview:         This function set process ChartsBar
 * Note:
 ********************************************************************/
function processScaleBar ($event, $scope, elem){
    var temp = elem.substring(0,4);
    if(temp == "Hudm"){
        $scope.globalVars.ClickSensorsBtn["HudmReq1Min"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar1Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar6Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar1Day"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar1Week"] = false;
        $scope.globalVars.ClickSensorsBtn["HudmBar3Month"] = false;
        $scope.globalVars.processBar.hudm.HudmBar1Hour = false;
        $scope.globalVars.processBar.hudm.HudmBar6Hour = false;
        $scope.globalVars.processBar.hudm.HudmBar1Day = false;
        $scope.globalVars.processBar.hudm.HudmBar1Week = false;
        $scope.globalVars.processBar.hudm.HudmBar3Month = false;
        $scope.globalVars.processBar.hudm.lastPos = elem; // save process bar position in global
        for (var key in $scope.globalVars.processBar.hudm) {//change all menu's buttons flags to false
            if (key == elem)
            {
                $scope.globalVars.processBar.hudm[key] = $event.originalEvent.isTrusted;
            }
        }
        if ($scope.globalVars.processBar.temp.lastPos != ""){
            $scope.tempDataStatus = 0; // load temperature data too after hudmunity load
            $scope.globalVars.ClickSensorsBtn[$scope.globalVars.processBar.temp.lastPos] = true;
            $scope.globalVars.ClickSensorsBtn.TempReq1Min = false;
        }
        else{
            $scope.tempDataStatus = 0; // load temperature data too after hudmunity load
            $scope.globalVars.ClickSensorsBtn.TempBar1Hour = true;
            $scope.globalVars.ClickSensorsBtn.TempReq1Min = false;
        }
        $scope.HudmDataStatus = 0;
        $scope.globalVars.dataSeries = [];
    }
    if(temp == "Temp"){
        $scope.globalVars.ClickSensorsBtn["TempReq1Min"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar6Hour"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Day"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar1Week"] = false;
        $scope.globalVars.ClickSensorsBtn["TempBar3Month"] = false;
        $scope.globalVars.processBar.temp.TempBar1Hour = false;
        $scope.globalVars.processBar.temp.TempBar6Hour = false;
        $scope.globalVars.processBar.temp.TempBar1Day = false;
        $scope.globalVars.processBar.temp.TempBar1Week = false;
        $scope.globalVars.processBar.temp.TempBar3Month = false;
        $scope.globalVars.processBar.temp.lastPos = elem; // save process bar position in global
        for (var key in $scope.globalVars.processBar.temp) {//change all menu's buttons flags to false
            if (key == elem)
            {
                $scope.globalVars.processBar.temp[key] = $event.originalEvent.isTrusted;
            }
        }
        if ($scope.globalVars.processBar.hudm.lastPos != ""){
            $scope.HudmDataStatus = 0; // load hudmunity data too after temperature load
            $scope.globalVars.ClickSensorsBtn[$scope.globalVars.processBar.hudm.lastPos] = true;
            $scope.globalVars.ClickSensorsBtn.HudmReq1Min = false;
        }
        else{
            $scope.hudmDataStatus = 0; // load hudmunity data too after temperature load
            $scope.globalVars.ClickSensorsBtn.HudmBar1Hour = true;
            $scope.globalVars.ClickSensorsBtn.TempReq1Min = false;
        }
        $scope.tempDataStatus = 0;
        $scope.globalVars.dataSeries = [];
    }
    $scope.globalVars.ClickSensorsBtn[elem] = $event.originalEvent.isTrusted;
    $scope.SensorBtnLastPassive = {};
    for (var key in $scope.globalVars.ClickSensorsBtn) {//change all menu's buttons flags to false
        if (key == elem)
        {
            $scope.SensorBtnLastPassive[key] = $event.originalEvent.isTrusted;
        }
    }
}

/*********************************************************************
 * Function:        sortByDateTime($scope, dataType, dataArray)
 * PreCondition:     sensors data received
 * Input:            dataType (warnins, alarms, events)
 * Return^
 * Overview:         This function return sorted data by DateTime (last value - first in array)
 * Note:
 ********************************************************************/
function sortByDateTime ($scope, dataType, dataArray){
    if (dataType == "Alarm") {
        //***************SORT ALARMS BY DATE&TIME //
        var tempAlarms = new Array();
        var alarms = new Array;
        for (j = 0; j < $scope.globalVars.alarms.err.length; j++) {//push elem to temp array
            alarms.push({});
            alarms[j]["Name"] = $scope.globalVars.alarms.err[j].Name;
            alarms[j]["Date"] = $scope.globalVars.alarms.err[j].Date;
            alarms[j]["Time"] = $scope.globalVars.alarms.err[j].Time;
        }
            if (alarms.length != 0) {
                for (j = 0; j < alarms.length; j++) {
                    alarms[j].Date = alarms[j].Date.split('-');
                    alarms[j].Date = alarms[j].Date[0] * 31536000 + alarms[j].Date[1] * 2629743 + alarms[j].Date[2] * 86400;
                    alarms[j].Time = alarms[j].Time.split(':');
                    alarms[j].Time = alarms[j].Time[0] * 3600 + alarms[j].Time[1] * 60 + alarms[j].Time[2];
                    alarms[j].Time = +alarms[j].Time;
                    alarms[j].Time = alarms[j].Time + alarms[j].Date;
                }
            }
            for (i = 0; i < alarms.length; i++) {
                var max = alarms[i];                //set max elem[0]
                var index = "";
                for (j = 0; j < alarms.length; j++) {//find really max elem
                    if (alarms[j].Time > max.Time) {
                        max = alarms[j];
                        index = j;                     //find index of max elem
                    }
                    if (index == "") {//find index of max elem
                        index = i;
                    }
                }
                if ($scope.globalVars.alarms.err[index].Name == alarms[index].Name) {
                    tempAlarms.push($scope.globalVars.alarms.err[index]);                      //push max elem #1 first in tempAlarms Array
                }
                else {
                    for (j = 0; j < $scope.globalVars.alarms.err.length; j++) {//push elem to temp array
                        if (alarms[j] != undefined) {
                            if (alarms[j].Name == $scope.globalVars.alarms.err[j].Name) {
                                tempAlarms.push($scope.globalVars.alarms.err[j]);                      //push max elem #1 first in tempAlarms Array
                            }
                        }
                    }
                }
                alarms.splice(index, 1);                   //delete max #1 from old array
                if (alarms.length == 1) {
                    for (j = 0; j < $scope.globalVars.alarms.err.length; j++) {//push elem to temp array
                        if (alarms[0] != undefined) {
                            if (alarms[0].Name == $scope.globalVars.alarms.err[j].Name) {
                                tempAlarms.push($scope.globalVars.alarms.err[j]);                      //push max elem #1 first in tempAlarms Array
                            }
                        }
                    }
                    break;
                }
            }
            $scope.globalVars.alarms.err = tempAlarms;
            return;
            // ********************************************************
    }
    if (dataType == "Warning"){
        //***************SORT WARNINGS BY DATE&TIME //
        var tempWarnings = new Array();
        var warnings = new Array;
        for (j = 0; j < $scope.globalVars.alarms.warning.length; j++){//push elem to temp array
            warnings.push({});
            warnings[j]["Name"] = $scope.globalVars.alarms.warning[j].Name;
            warnings[j]["Date"] = $scope.globalVars.alarms.warning[j].Date;
            warnings[j]["Time"] = $scope.globalVars.alarms.warning[j].Time;
        }

        if (warnings.length != 0){
            for (j = 0; j < warnings.length; j++){
                warnings[j].Date = warnings[j].Date.split('-');
                warnings[j].Date = warnings[j].Date[0]*31536000 + warnings[j].Date[1]*2629743 + warnings[j].Date[2] * 86400;
                warnings[j].Time = warnings[j].Time.split(':');
                warnings[j].Time = warnings[j].Time[0]*3600 + warnings[j].Time[1]*60 + warnings[j].Time[2];
                warnings[j].Time = +warnings[j].Time;
                warnings[j].Time =  warnings[j].Time +  warnings[j].Date;
            }
        }
        for(i = 0; i < warnings.length; i++){
            var max = warnings[i];                //set max elem[0]
            var index = "";
            for (j = 0; j < warnings.length; j++){//find really max elem
                if (warnings[j].Time > max.Time) {
                    max = warnings[j];
                    index = j;                     //find index of max elem
                }
                if (index == "") {//find index of max elem
                    index = i;
                }
            }
            if ($scope.globalVars.alarms.warning[index].Name == warnings[index].Name){
                tempWarnings.push($scope.globalVars.alarms.warning[index]);                      //push max elem #1 first in tempAlarms Array
            }
            else{
                for (j = 0; j < $scope.globalVars.alarms.warning.length; j++){//push elem to temp array
                    if(warnings[j] != undefined){
                        if (warnings[j].Name == $scope.globalVars.alarms.warning[j].Name){
                            tempWarnings.push($scope.globalVars.alarms.warning[j]);                      //push max elem #1 first in tempAlarms Array
                        }
                    }
                }
            }
            warnings.splice(index, 1);                   //delete max #1 from old array
            if(warnings.length == 1){
                for (j = 0; j < $scope.globalVars.alarms.warning.length; j++){//push elem to temp array
                    if(warnings[0] != undefined){
                        if (warnings[0].Name == $scope.globalVars.alarms.warning[j].Name){
                            tempWarnings.push($scope.globalVars.alarms.warning[j]);                      //push max elem #1 first in tempAlarms Array
                        }
                    }
                }
                break;
            }
        }
        $scope.globalVars.alarms.warning = tempWarnings;
        return;
        // ********************************************************
    }

}


/*********************************************************************
 * Function:        getSensorsLog($scope, $http, conf, getString)
 * PreCondition:     sensors data received
 * Input:            dataType (warnins, alarms, events)
 * Return^
 * Overview:         This function return sorted data by DateTime (last value - first in array)
 * Note:
 ********************************************************************/
function getSensorsLog ($scope, $http, conf, getString){
    var address;
    address = userInfo.ServerIP + userInfo.ServerPort + getString; // host + req string
    $http({
        method: 'PUT',
        url: address,
        data: conf,
    }).then(function(sensors) {
        $scope.globalVars.activeLog = sensors.data;
        if ($scope.globalVars.activeLog.length != 0)//process log data
        {
            $scope.globalVars.clickLogButtons.logDownloadOk = false;
            var logAvailableFlag = false;
            if ($scope.globalVars.activeLog.hudmLog.length != 0){
                for (i = 0; i < $scope.globalVars.activeLog.hudmLog.length; i++){
                    $scope.globalVars.activeLog.hudmLog[i].Date = $scope.globalVars.activeLog.hudmLog[i].Date.substr(0, 10);
                }
                $scope.globalVars.clickLogButtons.logDownloadOk = true;
                logAvailableFlag = true;
                if (language == "Russian"){
                    alert("Лог показаний влажности загружен!");
                }
                if (language == "English"){
                    alert ("Hudmunity Log downloaded!");
                }
            }
            if ($scope.globalVars.activeLog.tempLog.length != 0){
                for (i = 0; i < $scope.globalVars.activeLog.tempLog.length; i++){
                    $scope.globalVars.activeLog.tempLog[i].Date = $scope.globalVars.activeLog.tempLog[i].Date.substr(0, 10);
                }
                $scope.globalVars.clickLogButtons.logDownloadOk = true;
                logAvailableFlag = true;
                if (language == "Russian"){
                    alert("Лог показаний температуры загружен!");
                }
                if (language == "English"){
                    alert ("Temperature Log downloaded!");
                }
            }
            if ($scope.globalVars.activeLog.relayLog.length != 0){
                for (i = 0; i < $scope.globalVars.activeLog.relayLog.length; i++){
                    $scope.globalVars.activeLog.relayLog[i].Date = $scope.globalVars.activeLog.relayLog[i].Date.substr(0, 10);
                }
                $scope.globalVars.clickLogButtons.logDownloadOk = true;
                logAvailableFlag = true;
                if (language == "Russian"){
                    alert("Лог реле загружен!");
                }
                if (language == "English"){
                    alert ("Relay Log downloaded!");
                }
            }
            if ($scope.globalVars.activeLog.masterLog.length != 0){
                for (i = 0; i < $scope.globalVars.activeLog.masterLog.length; i++){
                    $scope.globalVars.activeLog.masterLog[i].Date = $scope.globalVars.activeLog.masterLog[i].Date.substr(0, 10);
                }
                $scope.globalVars.clickLogButtons.logDownloadOk = true;
                logAvailableFlag = true;
                if (language == "Russian"){
                    alert("Лог загружен!");
                }
                if (language == "English"){
                    alert ("Log downloaded!");
                }
            }
            if ($scope.globalVars.activeLog.slaveLog.length != 0){
                for (i = 0; i < $scope.globalVars.activeLog.slaveLog.length; i++){
                    $scope.globalVars.activeLog.slaveLog[i].Date = $scope.globalVars.activeLog.slaveLog[i].Date.substr(0, 10);
                }
                $scope.globalVars.clickLogButtons.logDownloadOk = true;
                logAvailableFlag = true;
                if (language == "Russian"){
                    alert("Лог загружен!");
                }
                if (language == "English"){
                    alert ("Log downloaded!");
                }
            }
            //fill brood log, add Type for all devices
            if (logAvailableFlag){
                var templog = new Array;
                for (var key in $scope.globalVars.activeLog){
                    for(j = 0; j< $scope.globalVars.activeLog[key].length; j++){
                        if (key == 'tempLog'){
                            templog.push($scope.globalVars.activeLog[key][j]);
                        }
                        if (key == 'hudmLog'){
                            if (language == "Russian"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Д. влажности";
                            }
                            if (language == "English"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Hudm. sensor";
                            }
                            templog.push($scope.globalVars.activeLog[key][j]);
                        }
                        if (key == 'relayLog'){
                            if (language == "Russian"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Реле";
                            }
                            if (language == "English"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Relay";
                            }
                            templog.push($scope.globalVars.activeLog[key][j]);
                        }
                        if (key == 'slaveLog'){
                            if (language == "Russian"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Узел";
                            }
                            if (language == "English"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Node";
                            }
                            templog.push($scope.globalVars.activeLog[key][j]);
                        }
                        if (key == 'masterLog'){
                            if (language == "Russian"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Главный";
                            }
                            if (language == "English"){
                                $scope.globalVars.activeLog[key][j]["Type"] = "Master";
                            }
                            templog.push($scope.globalVars.activeLog[key][j]);
                        }
                    }
                }
                $scope.globalVars.logLoadFlag = false;
                $scope.globalVars.broodLog = templog;
            }
        }
        else{
            $scope.globalVars.clickLogButtons.logDownloadOk = false;
            if (language == "Russian"){
                alert("Ошибка: сбой подключения к серверу!");
            }
            if (language == "English"){
                alert ("Error: server connection error!");
            }
        }
    },function (err) {
        console.log('Server not response: getSensorsLog ERR');
        console.log(err);
    });

}

