/**
 * Created by VenoM on 05.09.2017.
 */
app.factory('globalVars', function () {
    return {
        sensorsFinal: '',
        ClickEvents: {                  //sensor and alarm components buttons
            relaysClickEvent: false,
            sensorsClickEvent: false,
            sensorsClickMenu: false,
            alarmClickMenu: false,
            ClickShowErr: false,
            ClickShowWarning: false,
            ClickShowAllEvents: false,
        },
        ClickSensorsBtn: {
            hideSystemInfo: false,
        },
        clickLogButtons: {
            showLogData: false,
            showLogSettings: false,
            logDownloadOk: false,
        },
        processBar:{
            hudm: {
                lastPos: "",
                HudmBar1Hour: "",
                HudmBar6Hour: "",
                HudmBar1Day: "",
                HudmBar1Week: "",
                HudmBar3Month: "",
            },
            temp: {
                lastPos: "",
                TempBar1Hour: "",
                TempBar6Hour: "",
                TempBar1Day: "",
                TempBar1Week: "",
                TempBar3Month: "",
            },
        },
        alarms: {
            err: new Array(),
            warning: new Array(),
        },
        chart: {
            showMarkers: false,
            showValues: true,
        },
        SensorBtnLastPassive: "",
        sensorsCount: "",
        dataSeries: "",
        sensorDataType: "",
        language: "Russian",
        showLangComment: false,
        logdataStart: "",
        logdataEnd: "",
        logdataType: {              // log type request
            ds18b20: false,       //temperature
            H: false,       // hudmunity
            R: false,       // relays
            Slave: false,   // slave devides
            Master: false,   // slave devides
        },
        activeLog: "",      //loaded log
        broodLog: "",
        logLoadFlag: false,
        data: "",
        ngTableExpanded: "",

    }
});