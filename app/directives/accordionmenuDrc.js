/**
 * Created by VenoM on 28.03.2017.
 */
app.directive("accordionMenu", function () {
    return function (scope, element, attrs) {
        var data = scope[attrs["accordionMenu"]];
        if (angular.isArray(data.answers)) {
            var ulElem = angular.element("<ul>");
            element.append(ulElem);
            for (var i = 0; i < data.answers.length; i++) {
                var liElem = angular.element('<li>').addClass("fig").addClass("button");
                liElem.append(angular.element('<p>').text(data.answers[i].name));
                ulElem.append(liElem);
            }
        }
    }
});