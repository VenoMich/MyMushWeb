/**
 * Created by VenoM on 28.03.2017.
 */
/*********************************************************************
 * directive:         app.directive("sensorsTableTemplate"
 *
 * PreCondition:
 *
 * Input:            JSON
 * Return:           html template (sensors table) if data.success
 *
 * Overview:
 *
 * Note:
 ********************************************************************/
app.directive("relaysTableTemplate", function () {
    return {
        restrict: 'E',
        templateUrl: "pure-css-accordion-nav/relays-table.html",
        link: function (scope, element, attrs) {
            console.log('directive_relays')
        }
    };
});

/*********************************************************************
 * directive:         app.directive("sensorsTableTemplate"
 *
 * PreCondition:
 *
 * Input:            JSON
 * Return:           html template (sensors table) if data.success
 *
 * Overview:
 *
 * Note:
 ********************************************************************/
app.directive("sensorsTableTemplate", function () {
    return {
        restrict: 'E',
        templateUrl: "pure-css-accordion-nav/sensor-table.html",
        link: function (scope, element, attrs) {
            console.log('directive_sensors')
        }
    };
});

/*********************************************************************
 * directive:         app.directive("logTemplate", function ()
 *
 * PreCondition:
 *
 * Input:            JSON
 * Return:           html template (log table) in app-content
 *
 * Overview:
 *
 * Note:
 ********************************************************************/
app.directive("logTemplate", function () {
    return {
        restrict: 'E',
        templateUrl: "pure-css-accordion-nav/log-template.html",
        link: function (scope, element, attrs) {
            console.log('directive_logTemplate.html')
        }
    };
});


/*********************************************************************
 * directive:         app.directive("sensorsLi", function (getOnlineSensors)
 *
 * PreCondition:
 *
 * Input:            JSON
 * Return:           html template (sensors table) if data.success
 *
 * Overview:
 *
 * Note:
 ********************************************************************/
// app.directive("sensorsLi", function (getOnlineSensors) {
//     return function (scope, element, attrs) {
//         var sensorsAvailable = "";
//         getOnlineSensors.success(function(data) {
//             var sensorsAvailable = data;
//             var data = scope[attrs["sensorsLi"]];
//             var tableElem = angular.element("<table>").addClass("table_blur");
//             element.append(tableElem);
//             var trElemOne = angular.element('<tr>').addClass("table_blur");
//             var sensor = trElemOne.append(angular.element('<th>').text("sensor"));
//             var master =  trElemOne.append(angular.element('<th>').text("master Station"));
//             for(i=0; i<1; i++)
//             {
//                 var trElem = angular.element('<tr>').addClass("table_blur");
//                 var thElem = angular.element('<td>').text("bla");
//                 trElem.append(angular.element('<td>').text("bla1"))
//                 trElem.append(thElem);
//
//                 var trElemTwo = angular.element('<tr>');
//                 var thElemTwo = angular.element('<td>').text("blabla");
//                 trElemTwo.append(angular.element('<td>').text("blabla"))
//                 trElemTwo.append(thElemTwo);
//                 tableElem.append(trElemOne, trElem, trElemTwo);
//             }
//         });
//         getOnlineSensors.error(function(error) {
//         });
//     }
// });